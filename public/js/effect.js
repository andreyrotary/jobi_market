$(document).ready(function () {

    $('.drop_menu_js').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: false, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'right', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
    );

    $('.sender .drop_msg_js').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'right', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
    );

    $('.receiver .drop_msg_js').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
    );

    $('.long_search').material_chip({
        autocompleteOptions: {
            data: {
                'Apple': null,
                'Microsoft': null,
                'Google': null
            },
            limit: Infinity,
            minLength: 1
        },
        placeholder: 'Поиск должность',
        secondaryPlaceholder: 'Поиск еще должность'
    });

    $('.js-example-basic-multiple').select2(
        {
            placeholder: "Выберите опцию",
            language: {
                noResults: function (params) {
                    return "Результат не найден.";
                }
            }
        }
    );

    $('.js-free-multiple').select2(
        {
            freeInput: true,
            placeholder: "Введите должность",
            language: {
                noResults: function (params) {
                    return "Результат не найден.";
                }
            }
        }
    );

    $(".rainting_vacancy_slide").owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        nav: true,
        navText: ["<i class='material-icons'>keyboard_arrow_left</i>", "<i class='material-icons'>navigate_next</i>"]
    });

    $(".all_diplomas").owlCarousel({
        loop: true,
        margin: 15,
        responsive: {
            1000: {
                items: 4
            },
            1200: {
                items: 2
            }
        }
    });


    if ($(".aditional_foto .foto_slide").length > 0) {
        var a = $(".aditional_foto .foto_slide").length;
        var b = 38 / a;
        for (i = 0; i <= a; i++) {
            $(".aditional_foto .foto_slide:nth-child(" + i + ")").css("margin-right", b * i + "%");
            $(".aditional_foto .foto_slide:nth-child(" + i + ")").css("z-index", 999 + a - i);
        }
    }


    $('select').material_select();


    $('.birthdate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    // $('.show_social input').open();

    $('body').on("click", ".add_forwar", function () {
        $(".forward").addClass("active");
    });
    $(".disable_forward").on("click", function () {
        $(".forward").removeClass("active");
    });


    $(".select_search a").on("click", function () {
        $(".select_search a").removeClass("active");
        $(this).addClass("active");
    });


    $("#add_social_top").on("click", function () {
        $(".socials_icons_top .show_social").toggleClass("show_option");
        $(this).toggleClass("roatate_close");
    });
    $("#add_social_bottom").on("click", function () {
        $(".list_socials_all .show_social").toggleClass("show_option");
        $(this).toggleClass("roatate_close");
    });

    $("#add_social_bottom_modal").on("click", function () {
        $(".social_media_modal .show_social").toggleClass("show_option");
        $(this).toggleClass("roatate_close");
    });


    $("#add_language").on("click", function () {
        $(".language_block_select").toggleClass("active");
    });
    $("#hide_language_btn").on("click", function () {
        $(".language_block_select").toggleClass("active");
    });

    $("#add_skils").on("click", function () {
        $(".skils_block_select").toggleClass("active");
    });
    $("#hide_skils_btn").on("click", function () {
        $(".skils_block_select").toggleClass("active");
    });

    $("#do_mini_aside").on("click", function () {
        $("aside").toggleClass("mini_aside");
        $("main").toggleClass("maxi_main");
        $(".icon-menu").toggleClass("icon-close");
        $(this).toggleClass("active")
    });

    $("#right_setting_add, #add_setting_events, #add_setting_guest").on("click", function () {
        $(".content_main ").toggleClass("active_right_setting");
        $(".right_setting").toggleClass("active_setting");
    });

    $(" #add_setting_guest").on("click", function () {
        $("#test2 ").addClass("active");
    });

    $(".simple_events").on("click", function () {
        $(".content_main ").removeClass("active_right_setting");
        $(".right_setting").removeClass("active_setting");
    });

    $('.tooltipped').tooltip({delay: 50});

    $('.modal').modal();

    $('.period_work_select').daterangepicker({
        opens: "left",
    });


    $("#show_block_list").on("click", function () {
        $(".list_resume").addClass("block_list");
        $(".rerange_list_resume").removeClass("show_in_line");
        $(".rerange_resume").removeClass("xl12");
        $(".rerange_resume").addClass("xl3");
        $(".list_resume").removeClass("row_list");
        $("#show_row_list").removeClass("active");
        $(this).addClass("active");
    });

    $("#show_row_list").on("click", function () {
        $(".list_resume").removeClass("block_list");
        $(".list_resume").addClass("row_list");
        $(".rerange_list_resume").addClass("show_in_line");
        $(".rerange_resume").removeClass("xl3");
        $(".rerange_resume").addClass("xl12");
        $("#show_block_list").removeClass("active");
        $(this).addClass("active");
    });

    $("#show_events, #show_guest").on("click", function () {
        $(".guest_mini, .events_mini, .simple_events").removeClass("active");
        $(this).toggleClass("active");
        $("body").removeClass("hide_active");
        $("body").addClass("hide_active");
    });

    $("#show_events").on("click", function () {
        $(".content_main, .events_mini").addClass("active");
    });

    $("#show_guest").on("click", function () {
        $(".content_main, .guest_mini").addClass("active");
    });

    $(".slect_star").on("click", function () {
        $(this).toggleClass("active");
    });

    $(".hide_filter").on("click", function () {
        $(".fixed_filter_search").toggleClass("inactive");
        $(this).toggleClass("inactive");
    });

    $(".close_block_mini_top, #right_setting_add, .over_new").on("click", function () {
        $(".content_main, .events_mini, .guest_mini, .simple_events").removeClass("active");
        $("body").removeClass("hide_active");
    });


    if ($(".register_login_block").length > 0 || $(".no_aside").length > 0) {

        $('aside').css("display", "none");
    }

    if ($(".search_resume").length > 0) {

        $('.chips_search_resume').material_chip({
            autocompleteOptions: {
                data: {
                    'Manager': null,
                    'Sales Manager': null,
                    'Content Manager': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Поиск резюме',
            secondaryPlaceholder: 'Поиск еще резюме'
        });
    }

    /* NOUISLIDER Start */

    if ($(".content_resume").length > 0) {


        $('.chips').material_chip();
        $('.chips-placeholder').material_chip({
            data: [{
                tag: 'Manager',
            }, {
                tag: 'Sales manager',
            }],
            autocompleteOptions: {
                data: [{
                    'Microsoft': null,
                    'Google': null,
                    'GoDaddy': null
                }]
            },
            placeholder: 'Добавить работу',
            secondaryPlaceholder: 'Добавить еще работу'
        });

        var slider_english = document.getElementById('english');
        noUiSlider.create(slider_english, {
            start: 80,
            connect: [true, false],
            step: 1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 0,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });
        var skipValuesEnglish = [
            document.getElementById('value-english')
        ];
        slider_english.noUiSlider.on('update', function (values, handle) {
            skipValuesEnglish[handle].innerHTML = values[handle] + "%";
        });


        var slider_romanian = document.getElementById('romanian');
        noUiSlider.create(slider_romanian, {
            start: 94,
            connect: [true, false],
            step: 1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 0,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });
        var skipValuesRomanian = [
            document.getElementById('value-romanian')
        ];
        slider_romanian.noUiSlider.on('update', function (values, handle) {
            skipValuesRomanian[handle].innerHTML = values[handle] + "%";
        });

        var slider_design = document.getElementById('design');
        noUiSlider.create(slider_design, {
            start: 50,
            connect: [true, false],
            step: 1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 0,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });
        var skipValuesDesign = [
            document.getElementById('value-design')
        ];
        slider_design.noUiSlider.on('update', function (values, handle) {
            skipValuesDesign[handle].innerHTML = values[handle] + "%";
        });

        var slider_php = document.getElementById('php');
        noUiSlider.create(slider_php, {
            start: 25,
            connect: [true, false],
            step: 1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 0,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });
        var skipValuesPhp = [
            document.getElementById('value-php')
        ];
        slider_php.noUiSlider.on('update', function (values, handle) {
            skipValuesPhp[handle].innerHTML = values[handle] + "%";
        });


        var slider_salary = document.getElementById('salary-slider');
        noUiSlider.create(slider_salary, {
            start: [0, 1500],
            connect: true,
            step: 1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 0,
                'max': 1500
            },
            format: wNumb({
                decimals: 0
            })
        });

        var marginMin = document.getElementById('value-min-salary'),
            marginMax = document.getElementById('value-max-salary');

        slider_salary.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = "+" + values[handle] + "$";
            } else {
                marginMin.innerHTML = values[handle] + "$";
            }
        });

        $('.chips-language').material_chip({
            autocompleteOptions: {
                data: {
                    'Romanian': null,
                    'Russian': null,
                    'English': null,
                    'Germany': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите язык',
            secondaryPlaceholder: 'Выберите еще язык'
        });

        $('.chips-skils').material_chip({
            autocompleteOptions: {
                data: {
                    'PHP': null,
                    'Developer': null,
                    'Microsoft Office': null,
                    'Java': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите навыки',
            secondaryPlaceholder: 'Выберите еще навыки'
        });

        new Clipboard('.copy_ref');


    }
    /* NOUISLIDER End */


    if ($(".filter_preference").length > 0) {

        $('.chips-country').material_chip({
            autocompleteOptions: {
                data: {
                    'Romanian': null,
                    'Russian': null,
                    'English': null,
                    'Germany': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите страна',
            secondaryPlaceholder: 'Выберите еще страна'
        });

        $('.chips-city').material_chip({
            autocompleteOptions: {
                data: {
                    'Chisinau': null,
                    'Bucuresti': null,
                    'Balti': null,
                    'New York': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите город',
            secondaryPlaceholder: 'Выберите еще город'
        });

        $('.chips-region').material_chip({
            autocompleteOptions: {
                data: {
                    'Chisinau, Ciocana': null,
                    'Chisinau, Centru': null,
                    'Chisinau, Riscani': null,
                    'Chisinau, Telecentru': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите регион',
            secondaryPlaceholder: 'Выберите еще регион'
        });

        $('.chips-function').material_chip({
            autocompleteOptions: {
                data: {
                    'Manager': null,
                    'Sales Manager': null,
                    'Content Manager': null,
                    'Project Manager': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите должности',
            secondaryPlaceholder: 'Выберите еще должности'
        });

        $('.chips-company').material_chip({
            autocompleteOptions: {
                data: {
                    'Apple': null,
                    'Orange': null,
                    'Moldcell': null,
                    'Samsung': null
                },
                limit: Infinity,
                minLength: 1
            },
            placeholder: 'Выберите должности',
            secondaryPlaceholder: 'Выберите еще должности'
        });

        var slider_salary = document.getElementById('salary-slider');
        noUiSlider.create(slider_salary, {
            start: [0, 1500],
            connect: true,
            step: 1,
            orientation: 'horizontal', // 'horizontal' or 'vertical'
            range: {
                'min': 0,
                'max': 1500
            },
            format: wNumb({
                decimals: 0
            })
        });

        var marginMin = document.getElementById('value-min-salary'),
            marginMax = document.getElementById('value-max-salary');

        slider_salary.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = values[handle] + "$";
            } else {
                marginMin.innerHTML = values[handle] + "$";
            }
        });

    }

    $('.video_modal').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            inDuration: 300, // Transition in duration
            outDuration: 200, // Transition out duration
            startingTop: '4%', // Starting top style attribute
            endingTop: '10%', // Ending top style attribute
            ready: function (modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                document.getElementById('video_resume').play();
            },
            complete: function () {
                document.getElementById('video_resume').pause();
            } // Callback for Modal close
        }
    );

});


if ($(".saved_resume").length > 0) {
    var typed = new Typed('.element', {
        strings: ["Manager", "Sales Manager", "Content Manager"],
        loop: true,
        showCursor: true,
        startDelay: 1000,
        backDelay: 2000,
        typeSpeed: 50
    });
}

$(document).ready(resize_block);
$(window).on('resize', resize_block);

function resize_block() {
    wVideo = $(".block_video").width();
    wSlide = $(".content_diplomas").width();
    $(".block_video").height(wVideo * 1.206);
    $(".content_diplomas").height(wSlide * 1.208);
    $(".add_diplomas").height(wSlide * 1.208);

    var h_mini_rsume = $(".block_list .mini_resume:nth-child(1)").height();
    $(".add_resume_big_btn").css("height", h_mini_rsume);
}