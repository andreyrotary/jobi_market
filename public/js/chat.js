Vue.use(VueSocketio, 'http://'+window.location.hostname+':3000');
var socket = io.connect( 'http://'+window.location.hostname+':3000', {
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax : 5000,
    reconnectionAttempts: 99999
} );

(function($){
    $(function(){
        "use strict";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // function scroll(height, ele) {
        //     this.stop().animate({
        //         scrollTop: height
        //     }, 1, function() {});
        // };

        function chat_scroll(height){
            if(!height){
                height = $chat[0].scrollHeight;
            }
            $chat.stop().animate({
                        scrollTop: height
                    }, 1, function() {});
        }




        var $nickForm=$('#setNick');
        var $nickError=$('#nickError');
        var $nickBox=$('#nickname');
        var $users = $('#users');
        var $messageForm=$('#send-message');
        var $messageBox=$('#message');
        var $chat=$('.chat-body');
        var $destSocket = $('#destSocket');
        var $conversationId = $('#conversationId');
        var $fileupload = $('#fileupload');
        // var height = 0;
        var i;
        chat_scroll();


        var filesToRead = document.getElementById("filesupload");
        filesToRead.addEventListener("change", function(event) {
            //----------------------
            if(filesToRead.files.length != 0){
                // console.log(filesToRead.files.length);
                var msg = {};
                msg.files = true;
                msg.conversation_id = $conversationId.val();
                msg.user_id         = user_id;
                msg.text            = $messageBox.val();
                msg.destination     = $destSocket.val();
                processChatMessage(msg, filesToRead);
            }
            return false;
        }, false);

        // Process message before displaying
        function processChatMessage(msg, filesToRead){
            // console.log(msg);
            if(filesToRead){

                /*
                * add empty message for attachments
                * */
                var hostname = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;

                $.ajax({
                    type: "POST",
                    url: hostname + '/' + msg.conversation_id,
                    data: {message: _.escape(msg.text), user_id: user_id, username: username, conversation_id: msg.conversation_id, dest: msg.destination},
                    success: function( msg ) {

                        var files = filesToRead.files;
                        var len = files.length;
                        var url = baseUrl + '/messages/fileupload';
                        var file_data = filesToRead.files;
                        var form_data = new FormData();

                        var $attachements = $('<div class="message-attachments">');
                        for(var i=0; i<len; i++){
                            $attachements.append('<div class="message-attachment loading">loading</div>');
                        }
                        $chat.append(msg.html);
                        if($chat.find('.answer[id='+msg.id+']').length){
                            $chat.find('.answer[id='+msg.id+']').find('.text').append($attachements);
                        }
                        //---- add attachments ----

                        // console.log(msg);
                        Object.keys(msg).forEach(function (key) {
                            form_data.append(key, msg[key]);
                        });
                        console.log(file_data.length);

                        for (var i = file_data.length - 1; i >= 0; i--) {
                            // console.log('ok');

                            form_data.set('file', file_data[i]);
                            $.ajax({
                                url : url,
                                type : 'POST',
                                data : form_data,
                                processData: false,
                                contentType: false,
                                async: false,
                                success : function(data) {
                                    console.log(data);
                                    if(i==0){
                                        socket.emit('send message', {id: msg.id, newMessagesCount: msg.newMessagesCount, dest: msg.destination, conversationId: msg.conversation_id});
                                    }
                                    if($chat.find('.answer[id='+msg.id+']').length){
                                        $chat.find('.answer[id='+msg.id+']').find('.message-attachment').eq(i).removeClass('loading').html(data.attachment_html);
                                    }
                                }
                            });
                        }
                        //---- .add attachments ----

                        $messageBox.val('');
                    }
                });
                /*
                .
                * */

                $(this).val('');
                // console.log(form_data);
            } else if(msg.text){
                // console.log(msg.text);
                var hostname = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;

                $.ajax({
                    type: "POST",
                    url: hostname + '/' + msg.conversation_id,
                    data: {message: _.escape(msg.text), user_id: user_id, username: username, conversation_id: msg.conversation_id, dest: msg.destination},
                    success: function( msg ) {
                        // console.log(msg);

                        $.get(hostname+'/getpreviews/'+msg.id, function (data) {
                            if($chat.find('.answer[id='+msg.id+']').length){

                                $chat.find('.answer[id='+msg.id+']').find('.text').append(data);
                            }

                        });

                        socket.emit('send message', {id: msg.id, newMessagesCount: msg.newMessagesCount, dest: msg.destination, conversationId: msg.conversation_id});
                        $chat.append(msg.html);

                        $messageBox.val('');
                    }
                });
            }
        }


        if($chat.length){
            // height = $chat[0].scrollHeight;//height < $chat[0].scrollHeight ? $chat[0].scrollHeight : 0;
            // scroll.call($chat, height, this);
        }

        function send(){
            $messageForm.submit();
        }

        // socket.emit('new user', {username: username, user_id: user_id, user_avatar: user_avatar, user_email: user_email, user_email_hash: user_email_hash}, function(message){});


        //List of users
        // socket.on('usernames', function(data){
        //
        //     // console.log(data);
        //     var html = '';
        //     $('#users .user').removeClass('online');
        //     if(Object.keys(data).length>1){
        //         Object.keys(data).forEach(function(key){
        //             // if($('#users a[data-uid='+key+']')){
        //             // console.log(key);
        //             $('#users .user[data-uid='+key+']').addClass('online');
        //             // } else {
        //             //     $(this).removeClass('active');
        //             // }
        //         });
        //     }
        // });


        // $messageForm.submit(function(e){
        //     e.preventDefault();
        //     var msg = {};
        //     msg.files = false;
        //     msg.conversation_id = $conversationId.val();
        //     msg.text            = $messageBox.val();
        //     msg.destination     = $destSocket.val();
        //     processChatMessage(msg);
        //
        // });

        // socket.on('new message', function(message){
        //     // console.log(message);
        //     var hostname = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;
        //     if($conversationId.val() == message.conversationId) {
        //         $.get(hostname+'/render/'+message.id, function (data) {
        //             // console.log(data);
        //             $chat.append(data);
        //             if(!$chat.find('.answer[id='+message.id+']').hasClass('loaded')){
        //                 $.get(hostname+'/getpreviews/'+message.id, function (data) {
        //                     if($chat.find('.answer[id='+message.id+']').length){
        //                         $chat.find('.answer[id='+message.id+']').find('.text').append(data);
        //                     }
        //                     chat_scroll();
        //                 });
        //             }
        //
        //         });
        //     } else if($('#'+message.conversationId).length){
        //         if(message.newMessagesCount>0){
        //             var badge_count = (message.newMessagesCount<=9 ? message.newMessagesCount : '+9')
        //             $('#'+message.conversationId).find('.badge').text(badge_count);
        //         }
        //     }
        // });

        // socket.on('update messages status', function () {
        //     var hostname = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;
        //     $.get(hostname+'/get-status', function (data) {
        //         $('#new_count_message').text(data);
        //     });
        // });

        // Typing
        // socket.on('updateTyping', function(me, isTyping) {
        //     // console.log('typing');
        //     if (isTyping === true) {
        //         $('#typing').html(me + ' is typing...');
        //     } else {
        //         $('#typing').html('');
        //     }
        // });

        // $messageBox.keyup(function(e) {
        //     // console.log('typing');
        //     if (e.which === 13) {
        //         e.preventDefault();
        //         socket.emit('typing', {typing:false, from:socket.id, to: $destSocket.val()});
        //         send();
        //     } else if ($messageBox.val() !== '') {
        //         socket.emit('typing', {typing:true, from:socket.id, to: $destSocket.val()});
        //     } else {
        //         socket.emit('typing', {typing:false, from:socket.id, to: $destSocket.val()});
        //     }
        // });

        // $users.on('click', '.user', function() {
        //     $('.forwardtouser').each(function () {
        //         $(this).show();
        //     });
        //     $('#forwardto-'+$(this).data('uid')).hide();
        //     if ($conversationId.val() !== $(this).attr('id')) {
        //         var hostname = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;
        //         //--------------------------------------
        //         $.ajax({
        //             type: "GET",
        //             url: hostname + '/' + $(this).attr('id'),
        //             success: function (msg) {
        //                 // console.log(msg);
        //                 var i = 0;
        //                 $chat.html(msg.html);
        //                 var height = ($('.receiver:not(.is_seen):first').length ? $('.receiver:not(.is_seen):first').offset().top - $('.receiver:not(.is_seen):first').height : $chat[0].scrollHeight);
        //                 chat_scroll(height);
        //
        //                 $('.receiver').each(function(){
        //                     if(isScrolledIntoView($(this)) && !$(this).hasClass('is_seen')){
        //                         //-------------------------
        //                         var message_id = $(this).attr('id');
        //                         var $messageTextDiv = $(this).find('.text');
        //                         var message = $messageTextDiv.html();
        //                         // var is_seen = $(this).hasClass('is_seen')
        //                         $(this).addClass('inview');
        //                         var ajax_url = baseUrl + '/messages/fetch/'+message_id;
        //                         $.ajax({
        //                             type: 'POST',
        //                             url: ajax_url,
        //                             data: {text: message, user_id: user_id},
        //                             success: function (response) {
        //                                 $messageTextDiv.append(response.html);
        //                                 if($('#'+response.conversationId).data('uid')!=user_id){
        //                                     var badge_count = (response.newMessagesCount<=9 ? response.newMessagesCount : '+9');
        //                                     $('#'+response.conversationId).find('.badge').html(badge_count);
        //
        //                                     $('#new_count_message').text(response.newMessagesStatus);
        //                                 }
        //                             }
        //                         });
        //                         //-------------------------
        //                         $(this).addClass('is_seen');
        //                     }
        //                     else{
        //
        //                     }
        //                 });
        //             }
        //         });
        //         //--------------------------------------
        //         $users.find('.user').removeClass('active');
        //         $(this).addClass('active');
        //         $destSocket.val($(this).data('uid'));
        //         $conversationId.val($(this).attr('id'));
        //     }
        //     return false;
        // });

        $($chat).on('click', '.message-action', function (event) {
            event.preventDefault();
            var url = baseUrl + '/messages/delete',
                data = {},
                message_id = $(this).closest('.answer').attr('id');
            data.user_id = user_id;
            data.message_id = parseInt(message_id);
            data.type = $(this).data('type');
            // console.log($(this).attr('class'));
            $.ajax({
                url : url,
                type : 'POST',
                data : data,
                success : function(data) {
                    $('#'+message_id).html('<div class="text">').html('message deleted');
                    if(data.type == 'dfe'){
                        socket.emit('message delete for everyone', {from: socket.id, to: $destSocket.val(), message_id: data.message_id});
                    }
                }
            });
        });

        socket.on('message delete for everyone',function (data) {
            // console.log(data);
            $('#'+data.message_id).html('message deleted');
        });

        /*
        * Frorward message
        * */
        var $forwardForm = $('#message-forwarding');

        $($chat).on('click','.message-forward',function () {
            // $('#conversation_id').val($conversationId.val());
            $('#message_id').val($(this).closest('.answer').attr('id'));
            $('.message-fwd-modal').show();
        });

        $('.message-fwd-modal-close').click(function () {
            // $forwardForm[0].reset();
            $('.message-fwd-modal').hide();
        });


        $forwardForm.submit(function (e) {
            e.preventDefault();
            $('input[type=checkbox]').each(function() {
                if(this.checked){
                    var url = baseUrl + '/messages/forward/'+$('#message_id').val(),
                        data = {};
                        data.conversation_id = $(this).data('cid');
                        data.destination = $(this).data('uid');
                    $.ajax({
                        url : url,
                        type : 'POST',
                        data : data,
                        success : function(data) {
                            socket.emit('send message', {id: data.id, newMessagesCount: data.newMessagesCount, dest: data.destination, conversationId: data.conversation_id});
                        }
                    });
                }
            });
            $forwardForm[0].reset();
            $('.message-fwd-modal').hide();
        });
        /*
        * .Frorward message
        * */

        // $chat.scroll(function(){
        //     $('.receiver').each(function(){
        //         if(isScrolledIntoView($(this)) && !$(this).hasClass('is_seen')){
        //             //-------------------------
        //             var message_id = $(this).attr('id');
        //             var $messageTextDiv = $(this).find('.text');
        //             var message = $messageTextDiv.html();
        //             // var is_seen = $(this).hasClass('is_seen')
        //             $(this).addClass('inview');
        //             var ajax_url = baseUrl + '/messages/fetch/'+message_id;
        //             $.ajax({
        //                 type: 'POST',
        //                 url: ajax_url,
        //                 data: {text: message, user_id: user_id},
        //                 success: function (response) {
        //                     $messageTextDiv.append(response.html);
        //                     if($('#'+response.conversationId).data('uid')!=user_id){
        //                         var badge_count = (response.newMessagesCount<=9 ? response.newMessagesCount : '+9');
        //                         $('#'+response.conversationId).find('.badge').html(badge_count);
        //
        //                         $('#new_count_message').text(response.newMessagesStatus);
        //                     }
        //                 }
        //             });
        //             //-------------------------
        //             $(this).addClass('is_seen');
        //         }
        //         else{
        //
        //         }
        //     });
        // });



        // function isScrolledIntoView(elem){
        //     var $elem = $(elem);
        //     var $window = $(window);
        //
        //     var docViewTop = $window.scrollTop();
        //     var docViewBottom = docViewTop + $window.height();
        //
        //     var elemTop = $elem.offset().top;
        //     var elemBottom = elemTop + $elem.height();
        //
        //     return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        // }
    });
})(jQuery);