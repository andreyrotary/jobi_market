var chartData = generateChartData();

var chart1 = AmCharts.makeChart("chartdiv1", {
    "type": "serial",
    "theme": "light",

    "dataProvider": chartData,
    //"synchronizeGrid": true,
    "valueAxes": [{
        "id": "v1",
        "axisColor": "#203f5f",
        "axisThickness": 0,
        "axisAlpha": 0,
        "position": "left",
        "gridColor": "#e2e6ea",
        "gridAlpha": 1
    }],
    "graphs": [{
        "balloonText": "<div class='bulbe_graph'><div class='title_graph'>статистика просмотров</div><div class='inner_value_graph'><span>Количество просмотров</span><strong><i class='material-icons'>group</i>[[value]]</strong></div><div class='inner_data_graph'><span>Дата</span><strong>[[category]]</strong></div></div>",
        "valueAxis": "v1",
        "hideBulletsCount": 30,
        "title": "red line",
        "valueField": "visits",
        "fillColors": "#26abff",
        "fillAlphas": 0.7,
        "type": "smoothedLine"
    }],
    "balloon": {
        "adjustBorderColor": true,
        "cornerRadius": 0,
        "fillColor": "transparent",
        "fillAlpha": 1,
        "borderAlpha": 0,
        "shadowAlpha": 0,
        "showBullet": true

    },

    "chartCursor": {
        "categoryBalloonEnabled": true,
        "cursorAlpha": 0.1,
        "oneBalloonOnly": true
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "axisColor": "#203f5f",
        "minorGridEnabled": false,
        "gridAlpha": 0
    },
    "export": {
        "enabled": false
    }
});


function generateChartData() {
    var chartData = [];
    var firstDate = new Date();
    firstDate.setDate(firstDate.getDate() - 20);
    var visits = 600;
    for (var i = 0; i < 20; i++) {
        // we create date objects here. In your data, you can have date strings
        // and then set format of your dates using chart.dataDateFormat property,
        // however when possible, use date objects, as this will speed up chart rendering.
        var newDate = new Date(firstDate);
        newDate.setDate(newDate.getDate() + i);
        visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 65);
        chartData.push({
            date: newDate,
            visits: visits

        });
    }
    return chartData;
}


var chartData2 = generateChartData2();

var chart = AmCharts.makeChart("chartdiv2", {
    "type": "serial",
    "theme": "light",
    "dataProvider": chartData2,
    "valueAxes": [{
        "axisAlpha": 0,
        "gridColor": "#2a394f",
        "gridAlpha": 0.1,
        "dashLength": 0
    }],
    "gridAboveGraphs": true,
    "startDuration": 1,
    "graphs": [{
        "balloonText": "<div class='bulbe_graph'><div class='title_graph'>статистика сообщений</div><div class='inner_value_graph'><span>Количество сообщений</span><strong><i class='material-icons'>drafts</i>[[value]]</strong></div><div class='inner_data_graph'><span>Дата</span><strong>[[category]]</strong></div></div>",
        "lineAlpha": 0,
        "type": "column",
        "valueField": "visits",
        "fillColors": "#ff7a97",
        "fillAlphas": 1
    }],
    "balloon": {
        "adjustBorderColor": true,
        "cornerRadius": 0,
        "fillColor": "transparent",
        "fillAlpha": 1,
        "borderAlpha": 0,
        "shadowAlpha": 0,
        "showBullet": true

    },

    "chartCursor": {
        "categoryBalloonEnabled": true,
        "cursorAlpha": 0.1,
        "oneBalloonOnly": true
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "gridPosition": "start",
        "gridAlpha": 0,
        "tickPosition": "start",
        "tickLength": 5
    },
    "export": {
        "enabled": false
    }
});

function generateChartData2() {
    var chartData = [];
    var firstDate = new Date();
    firstDate.setDate(firstDate.getDate() - 20);
    var visits = 600;
    for (var i = 0; i < 20; i++) {
        // we create date objects here. In your data, you can have date strings
        // and then set format of your dates using chart.dataDateFormat property,
        // however when possible, use date objects, as this will speed up chart rendering.
        var newDate = new Date(firstDate);
        newDate.setDate(newDate.getDate() + i);
        visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 65);
        chartData.push({
            date: newDate,
            visits: visits

        });
    }
    return chartData;
}