<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

//Route::get('storage/{filename}', function ($filename)
//{
//    return Image::make(storage_path('uploads/' . $filename))->response();
//});

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'ConversationsController@index']);

    Route::get('get-status', ['as' => 'messages.get-status', 'uses' => 'ConversationsController@getStatus']);

    Route::get('get-new-messages-count', ['as' => 'messages.get-new-messages-count', 'uses' => 'ConversationsController@getNewMessagesCount']);

    Route::get('get-conversations', ['as' => 'messages.get-conversations', 'uses' => 'ConversationsController@getConversations']);
    Route::get('get-conversation-messages/{id?}', ['as' => 'messages.get-conversation-messages', 'uses' => 'ConversationsController@getConversationMessages']);

    Route::post('forward/{id}', ['as' => 'messages.forward', 'uses' => 'ConversationsController@forward']);

    Route::get('new-conversation/{id}', ['as' => 'messages.new.conversation', 'uses' => 'ConversationsController@newConversation']);

    Route::get('create', ['as' => 'messages.create', 'uses' => 'ConversationsController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'ConversationsController@store']);
    Route::post('fileupload', ['as' => 'messages.fileupload', 'uses' => 'ConversationsController@fileupload']);
    Route::post('fetch/{id?}', ['as' => 'messages.fetch', 'uses' => 'ConversationsController@fetch']);
    Route::post('delete', ['as' => 'messages.delete', 'uses' => 'ConversationsController@delete']);
    Route::get('render/{id}', ['as' => 'messages.render', 'uses' => 'ConversationsController@render']);
    Route::get('getpreviews/{id?}', ['as' => 'messages.getpreviews', 'uses' => 'ConversationsController@getpreviews']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'ConversationsController@show']);
    Route::post('{id?}', ['as' => 'messages.add', 'uses' => 'ConversationsController@add']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'ConversationsController@update']);
});

Route::group(['prefix' => 'api'], function () {
    //Route::get('countnewmessages', ['as' => 'api.count_new_messages', 'uses' => 'ApiController@count_new_messages']);
    Route::post('countnewmessages', ['as' => 'api.count_new_messages', 'uses' => 'ApiController@count_new_messages']);
});