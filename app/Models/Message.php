<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Message extends Model
{
    protected $table = 'messages';

    protected $touches = ['conversation'];

    public $timestamps = true;


    public $fillable = [
        'message',
        'previews',
        'is_seen',
        'deleted_from_sender',
        'deleted_from_receiver',
        'user_id',
        'conversation_id',
    ];

    /*
     * make dynamic attribute for human readable time
     *
     * @return string
     * */
    public function getHumansTimeAttribute()
    {
//        $datetime = array();

            $date = $this->created_at;
            $now = $date->now();
            $diff = $date->diffInHours($now, true);
            if($diff<=24){
                $datetime['time'] = $date->diffForHumans($now, true);
                $datetime['date'] = '';
            } else {
                $datetime['time'] = $date->format('H:i');
                $datetime['date'] = $date->format('j M. Y');
            }

        return $datetime;
    }

    /*
     * make a relation between conversation model
     *
     * @return collection
     * */
    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    /*
   * make a relation between user model
   *
   * @return collection
   * */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*
   * its an alias of user relation
   *
   * @return collection
   * */
    public function sender()
    {
        return $this->user();
    }

    /*
     * get all conversations with soft deleted message by given conversation id
     *
     * @param   int $conversationId
     * @param   int $offset
     * @param   int $take
     * @return  collection
     * */
    public function getMessagesAllById($conversationId, $user, $offset=0, $take=20)
    {
//                \DB::enableQueryLog();
        $messages = $this
            ->with(['messagesAttachments' => function ($query) {
                return $query->get();
            }])
            ->with('sender')
            ->where('conversation_id', $conversationId)
            ->where(function ($qq) use ($user){
                $qq->where(function ($qr) use ($user) {
                    $qr->where('user_id', $user);
                })
                    ->orWhere(function ($q) use ($user) {
                        $q->where('user_id', '!=', $user);
                    });
            })
            ->orderBy('created_at', 'DESC')
            ->offset($offset)
            ->limit($take)
            ->get();
        $messages_a = array();
        foreach ($messages as $m){
            $m->previews = json_decode($m->previews);
//            $m->sender->avatar = '/'.$m->sender->avatar;
            $messages_a[$m->id] = $m;
        }
//                $query = \DB::getQueryLog();
//        print_r($query);die;
//        dd($messages);
        return $messages_a;
    }

    public function newMessagesCount($conversationId, $user){
        $count = $this->where('conversation_id', $conversationId)
                    ->where('user_id', $user)
                    ->where('is_seen',0)->count();
        return $count;
    }

    /**
     * A message can have many attachments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messagesAttachments()
    {
        return $this->hasMany('App\Models\MessageAttachment', 'message_id');
    }

}
