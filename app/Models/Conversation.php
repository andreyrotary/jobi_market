<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Conversation extends Model
{
    protected $table = 'conversations';
    public $timestamps = true;
    public $authUser = 0;
    public $fillable = [
        'user_one',
        'user_two',
        'created_by',
        'status',
    ];

    /*
     * make a relation between message
     *
     * return collection
     * */
    public function messages()
    {
        return $this->hasMany('App\Models\Message', 'conversation_id')
            ->with('sender');
    }

    /*
     * make dynamic attribute for human readable time
     *
     * @return string
     * */
    public function getHumansTimeAttribute()
    {
        $date = $this->created_at;
        $now = $date->now();
        $diff = $date->diffInHours($now, true);
        $datetime = array();
        if($diff<=24){
            $datetime['time'] = $date->diffForHumans($now, true);
            $datetime['date'] = '';
        } else {
            $datetime['time'] = $date->format('H:i');
            $datetime['date'] = $date->format('j M. Y');
        }
        return $datetime;
    }

    public function messages_count()
    {
        return $this->hasMany('App\Models\Message', 'conversation_id')->count;
    }

    /*
     * make a relation between first user from conversation
     *
     * return collection
     * */
    public function userone()
    {
        return $this->belongsTo('App\User',  'user_one');
    }

    /*
   * make a relation between second user from conversation
   *
   * return collection
   * */
    public function usertwo()
    {
        return $this->belongsTo('App\User',  'user_two');
    }

    /**
     * fetch all inbox for currently loggedin user with pagination.
     *
     * @param int $offset
     * @param int $take
     *
     * @return array
     */

    /*
     * check this given user is exists
     *
     * @param   int $id
     * @return  bool
     * */
    public function existsById($id)
    {
        $conversation = $this->find($id);
        if ($conversation) {
            return true;
        }

        return false;
    }

    /*
     * check this given two users is already make a conversation
     *
     * @param   int $user1
     * @param   int $user2
     * @return  int|bool
     * */
    public function isExistsAmongTwoUsers($user1, $user2)
    {
        $conversation = Conversation::where('user_one', $user1)
            ->where('user_two', $user2);

        if ($conversation->exists()) {
            return $conversation->first()->id;
        }

        return false;
    }

    /*
     * check this given user is involved with this given $conversation
     *
     * @param   int $conversationId
     * @param   int $userId
     * @return  bool
     * */
    public function isUserExists($conversationId, $userId)
    {
        $exists = Conversation::where('id', $conversationId)
            ->where(function ($query) use ($userId) {
                $query->where('user_one', $userId)->orWhere('user_two', $userId);
            })
            ->exists();

        return $exists;
    }

    public function getInbox($user, $order = 'desc', $offset = 0, $take = 20)
    {
        return $this->threads($user, $order, $offset, $take);
    }

    public function getConversations($user, $order = 'desc', $offset = 0, $take = 20){
        $this->authUser = $user;
        $msgThread = $this->with(['messages' => function ($q) use ($user) {
            return $q->where(function ($q) use ($user) {
                $q->where('user_id', $user)
                    ->where('deleted_from_sender', 0);
            })
                ->orWhere(function ($q) use ($user) {
                    $q->where('user_id', '!=', $user);
                    $q->where('deleted_from_receiver', 0);
                })
                ->get();
        }, 'userone', 'usertwo'])
            ->where(function ($q) use ($user){
                $q->where('user_one', $user);
                $q->where('hidden_user_one', 0);
            })
            ->orWhere(function ($q) use ($user){
                $q->where('user_two', $user);
                $q->where('hidden_user_two', 0);
            })
//            ->where('user_one', $user)
//            ->orWhere('user_two', $user)
            ->offset($offset)
            ->take($take)
            ->orderBy('updated_at', $order)
            ->get();
        $threads = [];
        foreach ($msgThread as $thread) {
            if(
                ($thread->created_by === Auth::user()->id && $thread->messages->first()['message'] === null) ||
                ($thread->messages->first()['message'] !== null )
            ){
                $collection = (object) null;
                $collection->id = $thread->id;
                $collection->count = $thread->messages->where('is_seen',0)->where('user_id','!=', $user)->count();
                $collection->user_id = ($thread->userone->id == $user) ? $thread->usertwo->id : $thread->userone->id;
                $collection->name = ($thread->userone->id == $user) ? $thread->usertwo->name : $thread->userone->name;
                $collection->user_email = ($thread->userone->id == $user) ? $thread->usertwo->email : $thread->userone->email;
                $collection->user_avatar = ($thread->userone->id == $user) ? asset("storage").'/'.$thread->usertwo->avatar : asset("storage").'/'.$thread->userone->avatar;
                $collection->last_message = $thread->messages->first()['message'];
                $collection->last_message_date = ($thread->messages->first() ? $thread->messages->first()->getHumansTimeAttribute() : $thread->getHumansTimeAttribute());
                $collection->isActive = false;
                $collection->online = false;
                $collection->typing = false;
                $collection->updated_at = $thread->updated_at->timestamp;

                $threads[$collection->user_id] = $collection;
            }
        }

        return collect($threads);
    }

    /*
     * retrieve all message thread without soft deleted message with latest one message and
     * sender and receiver user model
     *
     * @param   int $user
     * @param   int $offset
     * @param   int $take
     * @return  collection
     * */
    public function threads($user, $order='desc', $offset=0, $take=10)
    {
        $this->authUser = $user;
        $msgThread = Conversation::with(['messages' => function ($q) use ($user) {
            return $q->where(function ($q) use ($user) {
                $q->where('user_id', $user)
                    ->where('deleted_from_sender', 0);
            })
                ->orWhere(function ($q) use ($user) {
                    $q->where('user_id', '!=', $user);
                    $q->where('deleted_from_receiver', 0);
                })
                ->get();
        }, 'userone', 'usertwo'])
            ->where('user_one', $user)
            ->orWhere('user_two', $user)
            ->offset($offset)
            ->take($take)
            ->orderBy('updated_at', $order)
            ->get();
//        dd($msgThread);
        $threads = [];
        foreach ($msgThread as $thread) {
            $collection = (object) null;
            $conversationWith = ($thread->userone->id == $user) ? $thread->usertwo : $thread->userone;
            $collection->id = $thread->id;
            $collection->count = $thread->messages->where('is_seen',0)->where('user_id','!=', $user)->count();
            $collection->thread = $thread->messages->first();
            $collection->withUser = $conversationWith;
            $threads[] = $collection;
        }

        return collect($threads);
    }

    /*
     * retrieve all message thread with latest one message and sender and receiver user model
     *
     * @param   int $user
     * @param   int $offset
     * @param   int $take
     * @return  collection
     * */
//    public function threadsAll($user, $offset=0, $take=10)
//    {
////        \DB::enableQueryLog();
//
//        $msgThread = Conversation::with(['messages' => function ($q) use ($user) {
//            return $q->latest();
//        }, 'userone', 'usertwo'])
////            ->where('is_seen', '=' 1)
//            ->where('user_one', $user)->orWhere('user_two', $user)->offset($offset)->take($take)->get();
////        $query = \DB::getQueryLog();
////        print_r($query);die;
//        $threads = [];
//        foreach ($msgThread as $thread) {
//            $conversationWith = ($thread->userone->id == $user) ? $thread->usertwo : $thread->userone;
//            $message = $thread->messages->first();
//            $message->user = $conversationWith;
//            $threads[] = $message;
//        }
//
//        return collect($threads);
//    }
    public function threadsAll($user, $offset=0, $take=10)
    {
//        \DB::enableQueryLog();

        $msgThread = Conversation::with(['messages' => function ($q) use ($user) {
            return $q->where('is_seen', '=', 0)
//                        ->where('user_id', '!=', $user)
                ->latest();
        }, 'userone', 'usertwo'])
//            ->where('is_seen', '=' 1)
            ->where('user_one', $user)->orWhere('user_two', $user)->offset($offset)->take($take)->get();
//        $query = \DB::getQueryLog();
//        print_r($query);die;
        $threads = [];
//        dd($msgThread);
        foreach ($msgThread as $thread) {
//            if(!empty($thread->messages)){
            $conversationWith = ($thread->userone->id == $user) ? $thread->usertwo : $thread->userone;
            $message = $thread->messages->where('is_seen', 0)->where('user_id', '!=', $user)->count();
//            dump($message);
            if($message>0){
                $message = $thread->messages->first();
                $message->user = $conversationWith;
                $threads[] = $message;
            }
        }

        return collect($threads);
    }

    /*
     * get all conversations with soft deleted message by given conversation id
     *
     * @param   int $conversationId
     * @param   int $offset
     * @param   int $take
     * @return  collection
     * */
    public function getMessagesAllById($conversationId, $user, $offset=0, $take=20)
    {
//        dd($conversationId);
//        \DB::enableQueryLog();
        $messages = $this->with(['messages' => function ($query) use ($offset, $take, $user) {
            return $query->orderBy('id', 'desc')->where(function ($qq) use ($user){
                $qq->where(function ($qr) use ($user) {
                    $qr->where('user_id', $user)->where('deleted_from_sender', 0);
                })
                    ->orWhere(function ($q) use ($user) {
                        $q->where('user_id', '!=', $user)->where('deleted_from_receiver', 0);
                    });
            })

                ->take($take);
        }, 'userone', 'usertwo'])
            ->with('')
            ->find($conversationId);
//                $queryy = \DB::getQueryLog();
//        print_r($queryy);die;
//        dd($messages);
        return $messages;
    }

    /*
     * get all conversations by given conversation id
     *
     * @param   int $conversationId
     * @param   int $userId
     * @param   int $offset
     * @param   int $take
     * @return  collection
     * */
    public function getMessagesById($conversationId, $userId, $offset=0, $take=10)
    {
        return Conversation::with(['messages' => function ($query) use ($userId, $offset, $take) {
            $query->where(function ($qr) use ($userId) {
                $qr->where('user_id', '=', $userId)
                    ->where('deleted_from_sender', 0);
            })
                ->orWhere(function ($q) use ($userId) {
                    $q->where('user_id', '!=', $userId)
                        ->where('deleted_from_receiver', 0);
                });

            $query->offset($offset)->take($take);

        }])->with(['userone', 'usertwo'])->find($conversationId);

    }

    /**
     * make a message as seen.
     *
     * @param int $messageId
     *
     * @return bool
     */
    public function makeSeen($messageId)
    {
        $seen = Message::update($messageId, ['is_seen' => 1]);
        if ($seen) {
            return true;
        }

        return false;
    }

    static function countNewMessages($user, $order='desc')
    {
//        Conversation::authUser = $user;
        $msgThread = Conversation::with(['messages' => function ($q) use ($user) {
            return $q->where(function ($q) use ($user) {
                $q->where('user_id', '!=', $user);
                $q->where('is_seen', 0);
            })
                ->latest();
        }, 'userone', 'usertwo'])
            ->where('user_one', $user)
            ->orWhere('user_two', $user)
//            ->offset($offset)
//            ->take($take)
            ->orderBy('updated_at', $order)
            ->get();

        $threads = [];
        foreach ($msgThread as $thread) {
            if($thread->messages->first()){
                $collection = (object) null;
                $conversationWith = ($thread->userone->id == $user) ? $thread->usertwo : $thread->userone;
                $collection->thread = $thread->messages->first();
                $collection->withUser = $conversationWith;
                $threads[] = $collection;
            }
        }
//        dd(count($threads));


        return collect($threads);
    }

    static function getCountMessages($user){
        return Conversation::countNewMessages($user)->count();
    }

    function countNewMessagesById($id, $user){
        return $this->messages->where('coversation_id', $id)->where('user_id', '!=', $user)->count();
    }

    //------------------------------------------------------------------------------------------------------------------




}
