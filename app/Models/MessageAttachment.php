<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageAttachment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'message_id'
    ];

    /**
     * A message attachment is owned by message
     */
    public function message(){
        return $this->belongsTo(Message::class);
    }
}
