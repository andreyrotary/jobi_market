<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\MessageAttachment;
use App\User;
use Illuminate\Http\Request;
use App\Models\Conversation;
use Illuminate\Support\Facades\Auth;
use View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
use Form;
use Dusterio\LinkPreview\Client;
use Carbon\Carbon;


class ConversationsController extends Controller
{
    /**
     * The ConversationRepository class instance.
     *
     * @var Conversation
     */
    protected $conversation;
    protected $message;

    /**
     * Currently loggedin user id.
     *
     * @var int
     */
    protected $authUserId;

    public function __construct(Conversation $conversation, Message $message){
        $this->middleware('auth');
        $this->conversation = $conversation;
        $this->message = $message;
        $this->setAuthUserId();
    }

    public function setAuthUserId()
    {
        $this->middleware(function ($request, $next) {
            $this->authUserId = Auth::user()->id;
            return $next($request);
        });
    }

    function index(){
//        dd($this->authUserId);
        $conversations = $this->conversation->getInbox(Auth::user()->id);
        $conversation_first = $this->message->getMessagesAllById($conversations->first()->id, Auth::user()->id);
//        $conversation_first = $conversation_first->reverse();
        $first_conversation_with = $conversations->first()->withUser->id;
        $first_conversation_id = $conversations->first()->id;
        $newmess = $this->conversation->threadsAll(Auth::user()->id);
        $messages = '';
//        if(!empty($conversation_first)){
//            foreach($conversation_first as $message){
//                $attachment_html = '';
//                if(!empty($message->messagesAttachments)){
//                    foreach ($message->messagesAttachments as $file){
//                        $file['img_mimes'] = array('jpeg','png','jpg','gif','svg');
//                        $file['video_mimes'] = array('mp4','ogx','oga','ogv','ogg','webm');
//                        $attachment_html .= view('messages.attachment')->with(['file'=>$file])->render();
//                    }
//
//                }
//                $forwarded = '';
//                $forwarded_a = json_decode($message->forwarded);
//                if(!empty($forwarded_a)){
//                    $forwarded_a->username = User::find($forwarded_a->user_id)->name;
//                    $forwarded = ($forwarded_a->username ? $forwarded_a->username : '') . ' - ' . ($forwarded_a->created_at ?  \Carbon\Carbon::parse($forwarded_a->created_at->date)->format('d-m-Y H:i') : '' );
//                }
////
//                $messages .= view('messages.message')->with(['message'=>$message, 'attachment_html'=>$attachment_html, 'forwarded'=>$forwarded])->render();
//            }
//        }
        $first_conversation = ['id'=>$first_conversation_id, 'with'=>$first_conversation_with, 'messages'=>$messages];
        return View::make('messages.chat')
            ->with(array('conversations' => $conversations, 'first_conversation'=> $first_conversation, 'newmess'=> $newmess ));
    }

    public function getStatus(){
        return $this->conversation->getCountMessages(Auth::user()->id);
    }

    public function getConversations(){
        $conversations = $this->conversation->getConversations(Auth::user()->id);
        return $conversations;
    }



    public function getConversationMessages($id){
        $conversation = $this->message->getMessagesAllById($id, Auth::user()->id);

	    return $conversation;
    }

    public function getNewMessagesCount(){
        return $this->conversation->getCountMessages(Auth::user()->id);
    }

    public function forward($id){
        $message = $this->message->with(['messagesAttachments' => function ($query) {
            return $query->get();
        }])->with('sender')->find($id);

        $m = new Message();
        $m->message = $message->message;
        $m->previews = $message->previews;
        $m->user_id = Auth::user()->id;
        $m->conversation_id = Input::get('conversation_id');
        $m->forwarded = json_encode(['user_id'=>$message->sender->id, 'created_at'=>$message->created_at]);
        $m->save();

        $attachment_html = '';
        if(!empty($message->messagesAttachments)){
            foreach ($message->messagesAttachments as $file){
                $at = new MessageAttachment();
                $at->filename = $file->filename;
                $at->mime = $file->mime;
                $at->original_filename = $file->original_filename;
                $at->message_id = $m->id;
                $at->save();
//                $file['img_mimes'] = array('jpeg','png','jpg','gif','svg');
//                $file['video_mimes'] = array('mp4','ogx','oga','ogv','ogg','webm');
//                $attachment_html .= view('messages.attachment')->with(['file'=>$at])->render();
            }

        }
//        $lastInserted       = $this->message->find($m->id);$attachment_html = '';
//        $lastInsertedHtml   = view('messages.message')->with(['message'=>$lastInserted, 'attachment_html'=>$attachment_html])->render();
        $arr['id']                     = $m->id;
        $arr['newMessagesCount']    = $this->message->newMessagesCount(Input::get('conversation_id'), Auth::user()->id);
        $arr['destination']             = Input::get('destination');
        $arr['conversation_id']         = Input::get('conversation_id');

        return response()->json($arr);

    }

    /**
     * Show conversation by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
//    function show($id){
//        $conversation = $this->message->getMessagesAllById($id, Auth::user()->id);
//        $conversation = $conversation->reverse();
//
//        $messages = '';
//        foreach($conversation as $message){
//            $attachment_html = '';
//            if(!empty($message->messagesAttachments)){
//                foreach ($message->messagesAttachments as $file){
//                    $file['img_mimes'] = array('jpeg','png','jpg','gif','svg');
//                    $file['video_mimes'] = array('mp4','ogx','oga','ogv','ogg','webm');
//                    $attachment_html .= view('messages.attachment')->with(['file'=>$file])->render();
//                }
//
//            }
//            $forwarded = '';
//            $forwarded_a = json_decode($message->forwarded);
//            if(!empty($forwarded_a)){
//                $forwarded_a->username = User::find($forwarded_a->user_id)->name;
//                $forwarded = ($forwarded_a->username ? $forwarded_a->username : '') . ' - ' . ($forwarded_a->created_at ?  \Carbon\Carbon::parse($forwarded_a->created_at->date)->format('d-m-Y H:i') : '' );
//            }
//
//            $messages .= view('messages.message')->with(['message'=>$message, 'attachment_html'=>$attachment_html, 'forwarded'=>$forwarded ])->render();
//        }
////        dd($messages);
////        $newmess = $this->conversation->threadsAll(Auth::user()->id);
//
//        $arr = array('html'=>$messages);
//        return response()->json( $arr );//json_encode($arr);
//    }

    /**
     * Add new message to database
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function add($id){
    	$has_previews = 0;
        $rules = array(
            'user_id'      => 'required|numeric',
            'conversation_id' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        $arr = [];
        if ($validator->fails()) {
            $arr['errors'] = $validator->errors()->all();
        } else {
	        if(!empty(Input::get('message'))){
		        preg_match_all("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",Input::get('message'),$a);
		        if(!empty($a[0])){
		        	$has_previews = 1;
		        }
	        }
            // store
            $message = new Message();
            $message->message = Input::get('message');
            $message->user_id = Input::get('user_id');
            $message->conversation_id = $id;
            $message->has_previews = $has_previews;
            $message->save();
//
            $lastInserted       = $this->message->with('sender')->find($message->id);
//            $attachment_html = '';
//            $forwarded = '';
//            $lastInsertedHtml   = view('messages.message')->with(['message'=>$lastInserted, 'attachment_html'=>$attachment_html, 'forwarded'=>$forwarded])->render();
//
            $arr['newMessagesCount']    = $this->message->newMessagesCount($id, Input::get('user_id'));
//            $arr['username']                = Input::get('username');
//            $arr['destination']             = Input::get('dest');
//            $arr['conversation_id']         = $id;
//            $arr['id']                     = $message->id;
//            $arr['user_id']                 = Input::get('user_id');
//            $arr['msg']                     = $message->message;
            $arr['message']                    = $lastInserted;


        }
        return response()->json($arr);
//        return $lastInserted;
    }

    /**
     * Message markup render
     *
     * @param $id
     */
    function render($id){
        $message       = $this->message->with(['messagesAttachments' => function ($query) {
            return $query->get();
        }])->find($id);
        $attachment_html = '';
        if(!empty($message->messagesAttachments)){
            foreach ($message->messagesAttachments as $file){
                $file['img_mimes'] = array('jpeg','png','jpg','gif','svg');
                $file['video_mimes'] = array('mp4','ogx','oga','ogv','ogg','webm');
                $attachment_html .= view('messages.attachment')->with(['file'=>$file])->render();
            }
        }

        $forwarded = '';
        $forwarded_a = json_decode($message->forwarded);
        if(!empty($forwarded_a)){
            $forwarded_a->username = User::find($forwarded_a->user_id)->name;
            $forwarded = ($forwarded_a->username ? $forwarded_a->username : '') . ' - ' . ($forwarded_a->created_at ?  \Carbon\Carbon::parse($forwarded_a->created_at->date)->format('d-m-Y H:i') : '' );
        }

        $messageHtml   = view('messages.message')->with(['message'=>$message, 'attachment_html'=>$attachment_html, 'forwarded'=>$forwarded])->render();

        return $messageHtml;
    }

    function delete(Request $request){
        $data = [];
        $message_id = $data['message_id'] = intval(Input::get('message_id'));
        $user_id    = $data['user_id']    = Input::get('user_id');
        $type       = $data['type']       = Input::get('type');
        $message = $this->message->find($message_id);
//        dd($message_id);

        if($user_id == $message['user_id']){
            if($type == 'dfm'){
                $data['status'] = $this->message->where('id',$message_id)->update(['deleted_from_sender' => 1]);
            } elseif ($type == 'dfe'){
                $data['status'] = $this->message->where('id',$message_id)->update(['deleted_from_sender' => 1, 'deleted_from_receiver' => 1,]);
            }
        } else {
            if($type == 'dfm'){
                $data['status'] = $this->message->where('id',$message_id)->update(['deleted_from_receiver' => 1]);
            }
        }


        return response()->json($data);
    }

    public function getpreviews($id){
//    	$update = $this->message->where('id', $id)->update(['has_previews'=>1]);
        $message = $this->message->find($id);
        $previews = array();
        if(!empty($message)){
            preg_match_all("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$message->message,$a);
            foreach($a[0] as $urls){
                $url = $urls;
                $previewClient = new Client($url);
                $preview = $previewClient->getPreview('general');
                $preview_inf = $preview->toArray();
                $preview_host = parse_url($url);
                $preview_host = $preview_host['scheme'].'://'.$preview_host['host'];
	            $previews[] = ['image'=>$preview_inf['cover'], 'title'=>$preview_inf['title'], 'url'=>$url, 'host'=>$preview_host];
            }
            $this->message->where('id', $id)->update(['previews'=>json_encode($previews)]);
        }
        return $previews;
    }


    /**
     * Fetch links from message and generate preview for each link
     *
     * @return string
     * @throws \Dusterio\LinkPreview\Exceptions\UnknownParserException
     * @throws \Throwable
     */
    public function fetch($id=0){
        $message = $this->message->find($id);
        $user_id = Input::get('user_id');

        if($message->user_id!==$user_id){
            $this->message->where('id',$id)->update(['is_seen' => 1]);
        }
        $arr['newMessagesCount']    = $this->message->newMessagesCount($message->conversation_id, $user_id);
        $arr['newMessagesStatus']   = $this->conversation->getCountMessages(Auth::user()->id);
        $arr['conversationId']      = $message->conversation_id;
        $arr['user_id']             = $message->user_id;

        return response()->json($arr);
    }


    /**
     * Send files as message
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fileupload(Request $request){
        $files = $request->file('files');
        $message_id = Input::get('id');
        $conversation_id = Input::get('conversation_id');

        $rules = array(
            'files.*'            => 'required|mimes:image/jpeg,jpeg,png,jpg,gif,svg,mp4,ogx,oga,ogv,ogg,webm,pdf,txt,csv,xlsx,doc,docx,ppt,pptx,ods,odt,odp,zip,rar|max:12000',
        );
        $validator = Validator::make(Input::all(), $rules);
//        $attachment_html = '';
        if ($validator->fails()) {

        } else {

            foreach($files as $file){
                if(is_file($file)){

                    $file_name = uniqid().'_'.time().'_'.date('Ymd');
                    $extension = $file->getClientOriginalExtension();

                    $file->storeAs('public/', $file_name.'.'.$extension);

                    $message_attachment = new MessageAttachment();
                    $message_attachment->filename = $file_name.'.'.$extension;
                    $message_attachment->mime = $extension;
                    $message_attachment->original_filename = $file->getClientOriginalName();
                    $message_attachment->message_id = $message_id;
                    $message_attachment->save();

                    $f = new \stdClass();
                    $f->filename = $file_name.'.'.$extension;
                    $f->mime = str_replace('.', '', $extension);
                    $f->img_mimes = array('jpeg','png','jpg','gif','svg');
                    $f->video_mimes = array('mp4','ogx','oga','ogv','ogg','webm');


//                    $attachment_html = view('messages.attachment')->with(['file'=>$f])->render();
                }
            }

        }
        $arr = [];
        $arr['newMessagesCount']    = $this->message->newMessagesCount($conversation_id, Input::get('user_id'));
        $m = $this->message->with('messagesAttachments')->with('sender')->find($message_id)->toArray();
//        dd($m);
        $arr['message'] = $m;
//        $arr['message']->at = $m;
//        $arr['message']->messages_attachments = json_decode(json_encode($m->messages_attachments[0]['filename']), FALSE);
        $arr['error']   = $validator->errors()->all();
//        dd(response()->json($arr));
        return response()->json($arr);


    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * make two users as serialize with ascending order.
     *
     * @param int $user1
     * @param int $user2
     *
     * @return array
     */
    protected function getSerializeUser($user1, $user2)
    {
        $user = [];
        $user['one'] = ($user1 < $user2) ? $user1 : $user2;
        $user['two'] = ($user1 < $user2) ? $user2 : $user1;

        return $user;
    }

    /**
     * make sure is this conversation exist for this user with currently loggedin user.
     *
     * @param int $userId
     *
     * @return bool|int
     */
    public function isConversationExists($userId)
    {
        if (empty($userId)) {
            return false;
        }

        $user = $this->getSerializeUser($this->authUserId, $userId);

        return $this->conversation->isExistsAmongTwoUsers($user['one'], $user['two']);
    }

    /**
     * make new conversation the given receiverId with currently loggedin user.
     *
     * @param int $receiverId
     *
     * @return int
     */
    protected function newConversation($receiverId)
    {
        $conversationId = $this->isConversationExists($receiverId);
        $user = $this->getSerializeUser($this->authUserId, $receiverId);

        if ($conversationId === false) {
            $conversation = $this->conversation->create([
                'user_one' => $user['one'],
                'user_two' => $user['two'],
                'created_by' => $this->authUserId,
                'status' => 1,
            ]);

            if ($conversation) {
                return redirect('messages');
            }
        }
//        return $conversationId;
        return redirect('messages');
    }



}
