var socket  = require( 'socket.io' );
var express = require('express');
var app     = express();
var http    = require('http');
var https   = require('https');
var request = require('request');

// var bodyParser = require('body-parser');
// app.use(bodyParser.json({limit: '100kb'}));

// var os      = require('os');
var server  = http.createServer(app);
var io      = socket.listen( server );
var port    = process.env.PORT || 3000;
var users = {},
    users_data = {};

// var aa = express.createSever();

// app.use(bodyParser);
// app.post('/public/countnewmessages/', function(req, res, body){
//     console.log(body);
//     var obj = {};
//     console.log('body: ' + JSON.stringify(req.body));
//     res.send(req.body);
// });

// request('http://localhost/jobi_market/public/countnewmessages/', { method: 'post', json: true }, function(err, res, body) {
//     if (err) { return console.log(err); }
//     // console.log(res);
//     console.log(body);
// // console.log(body.explanation);
// });

// var options = {
//     method: 'get',
//     uri: 'http://localhost/jobi_market/public/api/countnewmessages/'
// };
// request(options)
//     .then(function (response) {
//         console.log(response);
//         // Request was successful, use the response object at will
//     })
//     .catch(function (err) {
//         // console.log(err);
//         // Something bad happened, handle the error
//     })


// request.post({
//         followAllRedirects: true,
//         url:'http://localhost/jobi_market/public/countnewmessages/',
//         data: {id:1}
//     },
//     function(err,httpResponse,body){
//         console.log(body);
//     })

server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

io.set('heartbeat timeout', 4000);
io.set('heartbeat interval', 2000);


io.on('connection', function (socket) {

    /***
     Socket.io Events
     ***/

    // socket.on('join', function(data) {
    //     socket.join(data.room);
    //     socket.emit('joined', { message: 'Joined room: ' + data.room });
    // });

    // socket.on('new_user', function (data, callback) {
    //     data.user_id = parseInt(data.user_id);
    //     socket.username = data.username;
    //     socket.user_id = data.user_id;
    //     socket.user_avatar = data.user_avatar;
    //     socket.user_email = data.user_email;
    //     socket.user_email_hash = data.user_email_hash;
    //     users_data[] = {id:socket.id, user_id: data.user_id, username: socket.username, user_avatar: socket.user_avatar, user_email: socket.user_email, user_email_hash: socket.user_email_hash};
    //
    //     io.sockets.emit('users', users_data);
    // });

    socket.on('new_user', function (data, callback) {
        // console.log(data);
        // if(data.username in users){
        //     callback(false);
        // } else {
        //     callback(true);
            data.user_id = parseInt(data.user_id);
            socket.username = data.username;
            socket.user_id = data.user_id;
            socket.user_avatar = data.user_avatar;
            socket.user_email = data.user_email;
            socket.user_email_hash = data.user_email_hash;
            users[data.user_id] = {id:socket.id, user_id: data.user_id, username: socket.username, user_avatar: socket.user_avatar, user_email: socket.user_email, user_email_hash: socket.user_email_hash};
        // socket.emit('new_user', {user_id: data.user_id});
        updateNicknames();
        // }
    });

    socket.on('send_message', function (data) {
        // console.log(data);
        if(users[parseInt(data.dest)]){
        socketid = users[parseInt(data.dest)].id;
        sender = users[parseInt(data.dest)];
        // console.log(sender)
        // console.log(data.message + ' from: ' + socket.user_id);
        // if(socketid){
            if (io.sockets.connected[socketid]) {

                // console.log(socketid);
                io.sockets.connected[socketid].emit('new_message', {message: data.message, user_id: socket.user_id, conversationId: data.conversationId, newMessagesCount: data.newMessagesCount});
                // io.sockets.connected[socketid].emit('update messages status');
                // io.sockets.connected[socketid].emit('new messages count', {conversationId: data.conversationId});

                // console.log('from:'+socket.user_id);
                // console.log('dest:'+users[parseInt(data.dest)].user_id);
            }
        }
         // } //else {
            // io.sockets.emit('new message', {msg: data.message, nick: socket.nickname});
        // }
    });

    // socket.on('send message', function (data) {
    //     console.log(data);
    //
    //     if(users[parseInt(data.dest)]){
    //     socketid = users[parseInt(data.dest)].id;
    //     sender = users[parseInt(data.dest)];
    //     // console.log(sender)
    //     // console.log(data.message + ' from: ' + socket.user_id);
    //     // if(socketid){
    //         if (io.sockets.connected[socketid]) {
    //
    //             // console.log(socketid);
    //             io.sockets.connected[socketid].emit('new message', {id: data.id, conversationId: data.conversationId, });
    //             io.sockets.connected[socketid].emit('update messages status');
    //             // io.sockets.connected[socketid].emit('new messages count', {conversationId: data.conversationId});
    //
    //             // console.log('from:'+socket.user_id);
    //             // console.log('dest:'+users[parseInt(data.dest)].user_id);
    //         }
    //     }
    //      // } //else {
    //         // io.sockets.emit('new message', {msg: data.message, nick: socket.nickname});
    //     // }
    // });

    // is Typing
    socket.on('typing', function(data) {
        // console.log(data);
        // console.log(data.to);
        // console.log(users);

        // console.log(users[parseInt(data.to)]);
        if(users[parseInt(data.to)]){
            if (io.sockets.connected[users[parseInt(data.to)].id]) {
                // console.log('type');
                // io.sockets.connected[users[parseInt(data.to)].id].emit('updateTyping', socket.username, data.typing);
                io.sockets.connected[users[parseInt(data.to)].id].emit('updateTyping', {user_id: socket.user_id, typing: data.typing});
            }
        }
    });


    socket.on('message_dfe', function (data) {
        if(users[parseInt(data.to)]){
            if (io.sockets.connected[users[parseInt(data.to)].id]) {
                io.sockets.connected[users[parseInt(data.to)].id].emit('message_dfe', {id: data.id});
            }
        }
    });
    socket.on('message delete for everyone', function (data) {
        if(users[parseInt(data.to)]){
            if (io.sockets.connected[users[parseInt(data.to)].id]) {
                io.sockets.connected[users[parseInt(data.to)].id].emit('message delete for everyone', {message_id: data.message_id});
            }
        }
    });

    socket.on('disconnect', function (data) {
        // console.log(data);
        if(!socket.username) return;
        delete users[socket.user_id];
        updateNicknames();
        // socket.emit('user_disconnected', {user_id: socket.user_id});
    });

    function updateNicknames(){
        // console.log(users);
        io.sockets.emit('usernames', users);
    }

    // socket.on( 'new_count_message', function( data ) {
    //     // console.log(data);
    //     io.sockets.emit( 'new_count_message', {
    //         new_count_message: data.new_count_message
    //
    //     });
    // });

    // socket.on( 'update_count_message', function( data ) {
    //     // console.log(data);
    //     if (io.sockets.connected[users[parseInt(data.to)].id]) {
    //         io.sockets.connected[users[parseInt(data.to)].id].emit( 'update_count_message', {
    //             update_count_message: data.update_count_message
    //         });
    //     }
    // });

    socket.on('base64 file', function (data) {
        // console.log('received base64 file from' + msg.username);
        // console.log(data);

        if(users[parseInt(data.dest)]){
            socketid = users[parseInt(data.dest)].id;
            sender = users[parseInt(data.dest)];
            if (io.sockets.connected[socketid]) {
                io.sockets.connected[socketid].emit('base64 file',
                    {
                        id:socket.id,
                        user_id: socket.user_id,
                        username: socket.username,
                        user_avatar: socket.user_avatar,
                        user_email: socket.user_email,
                        user_email_hash: socket.user_email_hash,
                        files: data.files
                    }

                        );
            }
        }
    });

});
