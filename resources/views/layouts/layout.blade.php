<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @auth
        <script>
            user_id = {{ auth()->user()->id }};
            username = '{{ auth()->user()->name }}';
            user_avatar = '{{ auth()->user()->avatar }}';
            user_email = '{{ auth()->user()->email}}';
            user_email_hash = '{{ @md5(auth()->user()->email)}}';
            var baseUrl = "{{ url('/') }}";
            var token = "{{ csrf_token() }}";
        </script>
    @endauth
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import Awesome Icon Font-->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!--Import Awesome Icon Font-->
    <link href="{{ asset('css/icomons.css') }}" rel="stylesheet">
    <link
            href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
            rel="stylesheet">

    <!--Import owl carousel -->
    <link rel="stylesheet" href="{{ asset('css/owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owlcarousel/owl.theme.default.min.css') }}">

    <!--Import fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css"/>

    <!-- Import daterangepicker.css -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>

    <!-- Import nouislider.css -->
    <link href="{{ asset('css/nouislider.css') }}" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>



    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- AmChart CSS -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>

    <!--Import app.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app_assets.css') }}" media="screen,projection"/>

    <!--Import responsive.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/responsive.css') }}" media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>


<!-- Start Aside -->
<aside>

    <div class="content_top_logo">
        <div class="logo">
            <div class="top_logo">
                <a href="create_vacancy.php">jobi <span class="last_logo">market</span></a>
            </div>
        </div>
    </div>


    <div class="user_aside center-align">
        <div class="foto_user">
            <img src="{{ asset('images/20.jpg') }}" alt="">
        </div>
        <div class="name_user">Orange</div>
        <div class="email_user">resurse.umane@orange.md</div>
    </div>

    <div class="menu_aside">
        <div class="title_aside">menu</div>
        <div class="list_menu_aside">
            <ul>
                <li>
                    <a class="active waves-effect waves-blue" href="create_vacancy.php"><i class="material-icons">border_color</i><span>Создать вакансию</span></a>
                </li>
                <li>
                    <a href="list_vacancy.php" class="waves-effect waves-blue"><i
                                class="material-icons">business_center</i><span>Ваше вакансий</span></a></li>
                <li>
                    <a href="statistic.php" class="waves-effect waves-blue"><i class="material-icons">equalizer</i><span>Статистика вакансий</span></a>
                </li>
                <li>
                    <a href="message.php" class="waves-effect waves-blue"><i class="material-icons">local_post_office</i><span>Сообщений</span>
                        <span class="nr_count_left">+9</span></a>
                </li>
                <li>
                    <a href="preference.php" class="waves-effect waves-blue"><i class="material-icons">star</i><span>Избранные вакансий</span></a>
                </li>
                <li>
                    <a href="setting.php" class="waves-effect waves-blue"><i class="material-icons">settings</i><span>Настройка профиля</span></a>
                </li>
                <li>
                    <a href="vacancy.php" class="waves-effect waves-blue"><i
                                class="material-icons">first_page</i><span>Выход</span></a>
                </li>
            </ul>
        </div>
    </div>

    <div class="social_media_aside">
        <div class="title_aside">ПОДЕЛИТСА В СОЦ. СЕТЬ</div>
        <ul>
            <li><a href="#" class="waves-effect waves-blue color_facebook"><i class="fa fa-facebook-f"></i></a></li>
            <li><a href="#" class="waves-effect waves-blue color_twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="waves-effect waves-blue color_linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" class="waves-effect waves-blue color_ok"><i class="fa fa-odnoklassniki"></i></a></li>
            <li><a href="#" class="waves-effect waves-blue color_vk"><i class="fa fa-vk"></i></a></li>
        </ul>
    </div>

    <div class="slide_raiting_content center-align white-text">
        <!-- Set up your HTML -->
        <div class="owl-carousel rainting_vacancy_slide">
            <div>
                <div class="text_slide_rainting">
                    <div class="name_slide">“Вакансий №1”</div>
                    <div class="nr_rainting">имеет райтинг 3,5</div>
                </div>
                <div class="rating">
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star_half</i></span>
                    <span><i class="material-icons">star_border</i></span>
                </div>
            </div>
            <div>
                <div class="text_slide_rainting">
                    <div class="name_slide">“Вакансий №1”</div>
                    <div class="nr_rainting">имеет райтинг 4,5</div>
                </div>
                <div class="rating">
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star_half</i></span>
                </div>
            </div>
            <div>
                <div class="text_slide_rainting">
                    <div class="name_slide">“Вакансий №3”</div>
                    <div class="nr_rainting">имеет райтинг 2,5</div>
                </div>
                <div class="rating">
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star_half</i></span>
                    <span><i class="material-icons">star_border</i></span>
                    <span><i class="material-icons">star_border</i></span>
                </div>
            </div>
            <div>
                <div class="text_slide_rainting">
                    <div class="name_slide">“Вакансий №1”</div>
                    <div class="nr_rainting">имеет райтинг 3</div>
                </div>
                <div class="rating">
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star</i></span>
                    <span><i class="material-icons">star_border</i></span>
                    <span><i class="material-icons">star_border</i></span>
                </div>
            </div>


        </div>
    </div>

</aside>
<!-- End Aside -->

<!-- Start Main -->
<main>
    @include('partial.second_header')
    <div class="content_main page_main_full">

        <div class="top_page">
            <div class="row">
                <div class="col s12 l12">
                    <div class="title_page_top">Сообшение</div>
                </div>
            </div>
        </div>


        <div class="list_resume">
            <div class="row">
                <div class="col s12 l12">

@yield('content')



<!-- Modal Structure -->
<div id="modal2" class="modal">
    <button type="button" class="modal-action modal-close x_close"><i class="material-icons">close</i></button>
    <div class="modal-header center-align">
        <h3>Копируете уникальную сылку</h3>
    </div>
    <div class="modal-content">
        <div class="block_pop_up">
            <h4 class="valign-wrapper">Копируете уникальную сылку</h4>
            <p>Ваше св будет всегда тут, просто должны нажать на ссылку<br>
                Вы можете скопировать и послать в письме или сообщением любом</p>
            <div class="link_copy">
                <div id="foo" class="resume_link">https://jobicv.md/?resume=laurie.lowe13</div>
                <a id="clipboard-btn" class="waves-effect waves-light copy_ref" data-clipboard-target="#foo"
                   onclick="Materialize.toast('Успех! Скопировано в буфер обмена!', 2000)"><i
                            class="material-icons">content_copy</i></a></div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:;" class="modal-action modal-close waves-effect waves-blue btn-flat">Закрыть</a>
    </div>
</div>


<!-- Modal Structure -->
<div id="modal1" class="modal">
    <button type="button" class="modal-action modal-close x_close"><i class="material-icons">close</i></button>
    <div class="modal-header center-align">
        <h3>Вы успешно создали ваш резуме
            <strong>Поздравлаем!!!</strong></h3>
        <span class="title_rainting_modal">Ваш резюме имеет райтинг 3,5 <a href="javascript:;" class="tooltipped"
                                                                           data-position="right"
                                                                           data-delay="50"
                                                                           data-tooltip="Вкратце расскажите о своей деятельности. Описание работы, какие навыки приобрели."><i
                        class="material-icons">info_outline</i></a></span>
        <div class="rating">
            <span><i class="material-icons">star</i></span>
            <span><i class="material-icons">star</i></span>
            <span><i class="material-icons">star</i></span>
            <span><i class="material-icons">star_half</i></span>
            <span><i class="material-icons">star_border</i></span>
        </div>
    </div>
    <div class="modal-content">


        <div class="block_pop_up">
            <h4 class="valign-wrapper"><span class="nr">1</span>Ваша уникальная ссылка</h4>
            <p>Ваше св будет всегда тут, просто должны нажать на ссылку<br>
                Вы можете скопировать и послать в письме или сообщением любом</p>
            <div class="link_copy">
                <div id="foo" class="resume_link">https://jobicv.md/?resume=laurie.lowe13</div>
                <a id="clipboard-btn" class="waves-effect waves-light copy_ref" data-clipboard-target="#foo"
                   onclick="Materialize.toast('Успех! Скопировано в буфер обмена!', 2000)"><i
                            class="material-icons">content_copy</i></a></div>
        </div>

        <div class="block_pop_up">
            <h4 class="valign-wrapper"><span class="nr">2</span>Поделитесь в соц сети</h4>
            <p>Вы получите лайки и перепосты от ваших друзей<br>Это также повлияет на ваш рейтинг + 5</p>
            <div class="social_media_modal">
                <ul class="valign-wrapper">
                    <li><a href="#" class="waves-effect waves-blue color_facebook"><i class="fa fa-facebook-f"></i></a>
                    </li>
                    <li><a href="#" class="waves-effect waves-blue color_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="waves-effect waves-blue color_linkedin"><i class="fa fa-linkedin"></i></a>
                    </li>
                    <li><a href="#" class="waves-effect waves-blue color_ok"><i class="fa fa-odnoklassniki"></i></a>
                    </li>
                    <li><a href="#" class="waves-effect waves-blue color_vk"><i class="fa fa-vk"></i></a></li>
                    <li>
                        <button id="add_social_bottom_modal" class="dropdown-button add_social waves-effect waves-blue"
                                type="button"><i class="material-icons">add</i>
                        </button>
                        <div class="show_social">
                            <div class="select-wrapper"><span class="caret">▼</span><input type="text"
                                                                                           class="select-dropdown"
                                                                                           readonly="true"
                                                                                           data-activates="select-options-8b56c8ef-e99f-ce7a-0fcd-d8fe863ad83e"
                                                                                           value="Twitter">
                                <ul id="select-options-8b56c8ef-e99f-ce7a-0fcd-d8fe863ad83e"
                                    class="dropdown-content select-dropdown ">
                                    <li class=""><span>Twitter</span></li>
                                    <li class=""><span>LinkedIn</span></li>
                                    <li class=""><span>Ok.ru</span></li>
                                </ul>
                                <select data-select-id="8b56c8ef-e99f-ce7a-0fcd-d8fe863ad83e" class="initialized">
                                    <option value="1">Twitter</option>
                                    <option value="2">LinkedIn</option>
                                    <option value="3">Ok.ru</option>
                                </select></div>
                            <input class="txt_select" type="text" value="laurie.lowe13">
                            <button class="waves-effect waves-blue btn z-depth-1" type="submit">Добавить
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="block_pop_up">
            <h4 class="valign-wrapper"><span class="nr">3</span>Распечатайте</h4>
            <p>На собеседование можете распечатать и ваше персональное</p>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="left btn_modal_footer waves-effect waves-blue">Распечатайте</button>
        <button type="button" class="left btn_modal_footer waves-effect waves-blue">Сохранить PDF</button>
        <a href="javascript:;" class="modal-action modal-close waves-effect waves-blue btn-flat">Закрыть</a>
    </div>
</div>

<!-- Modal Structure -->
<div id="politika" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="txt_politica">
            <h2>Политика конфиденциальность</h2>
            <h3>На что распространяется Соглашение о конфиденциальности информации?</h3>
            <p>Это Соглашение распространяется на способ использования www.usagreencard.com собранной и
                полученной персональной информации о пользователях. Персональная информация - это
                информация, которая может быть соотнесена с личностью пользователя (как то ваше имя,
                адрес,
                адрес электронной почты, или номер телефона) и недоступна из других открытых
                источников.</p>

            <h3>Общие сведения</h3>
            <p>www.usagreencard.com собирает персональную информацию о Вас, когда вы регистрируетесь в
                качестве участника в иммиграционной программе Diversity Immigration Visa Program.
                При Вашей регистрации на www.usagreencard.com мы запрашиваем некоторые персональные
                данные,
                как то ваши имя и фамилию, адрес местожительства, адрес электронной почты, дату
                рождения,
                пол, почтовый индекс, а также информацию о вашей профессии, сфере занятий и др., а также
                фотографию. Как только вы регистрируетесь на www.usagreencard.com и соглашаетесь на наши
                услуги, вы перестаете быть для нас анонимным пользователем.
                www.usagreencard.com использует персональную информацию для следующих общих целей:
                подача
                зарегистрированных заявлений на участие в иммиграционной программе Diversity Immigration
                Visa Program, исследований и предоставления анонимной отчетности для внешних и
                внутренних
                пользователей.
                Заполняя анкету на участие в иммиграционной программе Diversity Immigration Visa Program
                и
                подтверждая согласие с условиями сайта www.usagreencard.com, принадлежащий компании WBCS
                GROUP, LLC, вы автоматически соглашаетесь с условиями обслуживания.</p>

            <h3>Дети</h3>
            <p>При попытке детей до 16 лет пройти регистрацию самостоятельно в www.usagreencard.com, мы
                автоматически аннулируем эти заявления.</p>

            <h3>Передача и раскрытие информации</h3>
            <p>www.usagreencard.com не продает и не передает персональную информацию о пользователях
                другим
                лицам или не связанным с нами компаниям, за исключением случаев, когда
                www.usagreencard.com
                обеспечивает запрошенные Вами услуги или когда мы располагаем вашим согласием на
                вышеуказанное использование информации.</p>

            <h3>Изменения в Соглашении о конфиденциальности информации</h3>
            <p>www.usagreencard.com может вносить изменения в Соглашение о конфиденциальности без
                предварительного уведомления пользователя. Вы можете ознакомиться с последней редакцией
                Соглашения о конфиденциальности в любое время по адресу:
                https://www.usagreencard.com/index.php?pid=privacy</p>

            <h3>Конфиденциальность и безопасность</h3>
            <p>Мы даем ограниченный доступ к вашей персональной информации только тем сотрудникам
                компании
                WBCS GROUP, LLC, которым, по нашему мнению, действительно необходимо использовать вашу
                информации для того, чтобы предоставить вам услуги по вашему запросу, или чтобы
                выполнять
                свои служебные обязанности. Чтобы защитить вашу персональную информацию, мы
                придерживаемся
                физических, электронных и процессуальных мер безопасности, которые соответствуют
                требованиям
                действующего законодательства.</p>

            <p>Контактные данные которые вы указываете в анкете будут использоваться для связи с вами по
                вопросу участия в программе Green Card условий участия и указаных вами персональных
                данных.
                Для этого мы будем использовать ваш адрес электронной почты и номер телефона. Вам будут
                приходить емайл рассылки, смс оповещение и телефонные звонки.</p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect">Я согласен</a>
    </div>
</div>

<div class="events_mini block_mini_top">
    <button class="close_block_mini_top" type="button"><i class="material-icons">close</i></button>
    <h3>Уведомления (5)</h3>
    <div class="tabs_events">
        <div class="list_setting_all">
            <ul class="tabs">
                <li class="tab col s3"><a class="active"
                                          href="#new_events">Новые <span class="nr_count_left">5</span></a>
                </li>
                <li class="tab col s3"><a href="#old_events">Входяшие</a></li>
            </ul>
        </div>
        <div id="new_events">
            <div class="list_events_mini">
                <div class="mini_events">
                    <span class="round_item"></span>
                    <div class="left_mini_events">
                        <span class="ora">19:33</span>
                        <span class="data">17 ноя. 2017</span>
                    </div>
                    <div class="right_mini_events">
                        <figure>
                            JM
                        </figure>
                        <p>Мы думаем, что вам могут понравиться эти вакансий
                            <a href="#">manager</a>, <a href="#">sales manager</a></p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="mini_events">
                    <span class="round_item"></span>
                    <div class="left_mini_events">
                        <span class="ora">19:33</span>
                        <span class="data">17 ноя. 2017</span>
                    </div>
                    <div class="right_mini_events">
                        <figure>
                            <img src="{{ asset('images/17.png') }}" alt="">
                        </figure>
                        <p>Посмотрите, какая находка есть у пользователя <strong>Moldcell</strong></p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="mini_events">
                    <span class="round_item"></span>
                    <div class="left_mini_events">
                        <span class="ora">19:33</span>
                        <span class="data">17 ноя. 2017</span>
                    </div>
                    <div class="right_mini_events">
                        <figure>
                            JM
                        </figure>
                        <p>Рекомендации <strong>JobiMarket</strong> <a href="#">Несколько (18)</a> свежих вакансий
                            из категории «дизайн» 1 дней назад</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="mini_events">
                    <span class="round_item"></span>
                    <div class="left_mini_events">
                        <span class="ora">19:33</span>
                        <span class="data">17 ноя. 2017</span>
                    </div>
                    <div class="right_mini_events">
                        <figure>
                            <img src="{{ asset('images/18.jpg') }}" alt="">
                        </figure>
                        <p><strong>Orange</strong> добавил <a href="#">ваш резюме</a> в избраное</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="mini_events">
                    <span class="round_item"></span>
                    <div class="left_mini_events">
                        <span class="ora">19:33</span>
                        <span class="data">17 ноя. 2017</span>
                    </div>
                    <div class="right_mini_events">
                        <figure>
                            <img src="{{ asset('images/19.jpg') }}" alt="">
                        </figure>
                        <p><strong>Apple</strong> создал вакансий (<a href="#">manager</a>, <a href="#">sales
                                manager</a>)
                            который вам может интересуетса</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <a class="btn_event waves-effect waves-blue " href="events.php">Показать все уведомления</a>
        </div>
        <div id="old_events">
            <div class="list_events_mini">
                <div class="mini_events">
                    <span class="round_item"></span>
                    <div class="left_mini_events">
                        <span class="ora">19:35</span>
                        <span class="data">18 ноя. 2017</span>
                    </div>
                    <div class="right_mini_events">
                        <button type="button" class="delete_event"><i class="material-icons">close</i>
                        </button>
                        <figure>
                            JM
                        </figure>
                        <p>Мы думаем, что вам могут понравиться эти вакансий
                            <a href="#">manager</a>, <a href="#">sales manager</a></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <a class="btn_event waves-effect waves-blue " href="events.php">Показать все уведомления</a>
        </div>
    </div>
</div>

<div class="guest_mini block_mini_top">
    <button class="close_block_mini_top" type="button"><i class="material-icons">close</i></button>
    <h3>Мои гости (15)</h3>
    <div class="content_mini_guest">
        <div class="item_mini_guest">
            <div class="block_mini_guest">
                <div class="logo_name_mini">
                    <figure>
                        A
                    </figure>
                    <span>Apple</span>
                </div>
                <div class="details_mini_guest">
                    <em>I will appreciate the fact that you are writing to me...</em>
                    <div class="action_mini_guest">
                        <a class="btn_vacancy_guest" href="#">вакансий (5)</a>
                        <a class="write_guest_msg" href="#">сообщение</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="time_guest_mini">
                19:33, 17 ноя. 2017
            </div>
        </div>
        <div class="item_mini_guest">
            <div class="block_mini_guest">
                <div class="logo_name_mini">
                    <figure>
                        A
                    </figure>
                    <span>Apple</span>
                </div>
                <div class="details_mini_guest">
                    <em>I will appreciate the fact that you are writing to me...</em>
                    <div class="action_mini_guest">
                        <a class="btn_vacancy_guest" href="#">вакансий (5)</a>
                        <a class="write_guest_msg" href="#">сообщение</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="time_guest_mini">
                19:33, 17 ноя. 2017
            </div>
        </div>
        <div class="item_mini_guest">
            <div class="block_mini_guest">
                <div class="logo_name_mini">
                    <figure>
                        A
                    </figure>
                    <span>Apple</span>
                </div>
                <div class="details_mini_guest">
                    <em>I will appreciate the fact that you are writing to me...</em>
                    <div class="action_mini_guest">
                        <a class="btn_vacancy_guest" href="#">вакансий (5)</a>
                        <a class="write_guest_msg" href="#">сообщение</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="time_guest_mini">
                19:33, 17 ноя. 2017
            </div>
        </div>
        <div class="item_mini_guest">
            <div class="block_mini_guest">
                <div class="logo_name_mini">
                    <figure>
                        A
                    </figure>
                    <span>Apple</span>
                </div>
                <div class="details_mini_guest">
                    <em>I will appreciate the fact that you are writing to me...</em>
                    <div class="action_mini_guest">
                        <a class="btn_vacancy_guest" href="#">вакансий (5)</a>
                        <a class="write_guest_msg" href="#">сообщение</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="time_guest_mini">
                19:33, 17 ноя. 2017
            </div>
        </div>
        <div class="item_mini_guest">
            <div class="block_mini_guest">
                <div class="logo_name_mini">
                    <figure>
                        A
                    </figure>
                    <span>Apple</span>
                </div>
                <div class="details_mini_guest">
                    <em>I will appreciate the fact that you are writing to me...</em>
                    <div class="action_mini_guest">
                        <a class="btn_vacancy_guest" href="#">вакансий (5)</a>
                        <a class="write_guest_msg" href="#">сообщение</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="time_guest_mini">
                19:33, 17 ноя. 2017
            </div>
        </div>
    </div>
    <div class="option_mini_guest">
        <div class="left">
            <a  class="more_guest" href="guest.php">Еше 10 новых гостей</a>
        </div>
        <div class="right"><a class="btn_event waves-effect waves-blue " href="guest.php">Все мои гости</a></div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="over_new"></div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
<script src="{{ asset('js/owlcarousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="{{ asset('js/graph_chart.js') }}"></script>

<script src="{{ asset('js/nouislider.js') }}"></script>
<script src="{{ asset('js/wNumb.js') }}"></script>
<script src="{{ asset('js/clipboard.js') }}"></script>
<script src="{{ asset('js/typed.js') }}"></script>
<script src="{{ asset('js/effect.js') }}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script src="https://unpkg.com/in-view@0.6.1/dist/in-view.min.js"></script>
<script src="{{ asset('js/vue-socketio.min.js') }}"></script>
{{--<script src="{{ asset('js/vue-truncate.js') }}"></script>--}}
<script src="https://unpkg.com/vue-observe-visibility@0.3.1"></script>
@yield('js')
</body>
</html>