<div  class="answer {{@($message->previews != '' ? 'loaded' : '' )}} @if (Auth::user()->id == $message->sender->id) right sender @else left receiver @endif {{@($message->is_seen == 1 ? 'is_seen' : '')}}" id="{{$message->id}}">
    <div class="message-actions btn-group">
        <div class="btn-group inline pull-left">
            <a class="message-forward btn btn-secondary" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-share" aria-hidden="true"></i>
            </a>
        </div>
        <div class="btn-group inline pull-left dropdown">

                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a data-type="dfm" class="message-action dropdown-item" href="#">Delete for myself</a></li>
                    @if (Auth::user()->id == $message->sender->id)<li><a data-type="dfe" class="message-action dropdown-item" href="#">Delete for everyone</a></li>@endif
                </ul>
        </div>
    </div>
    <div class="avatar">
        <img src="https://www.gravatar.com/avatar/{{@md5($message->sender->email)}}" alt="{{$message->sender->name}}">
        {{--<div class="status offline"></div>--}}
    </div>
    <div class="name">{{$message->sender->name}}</div>
    <div class="text">
        {{$message->message}}
        <div class="message-attachments">{!! $attachment_html !!}</div>
        {!! $message->previews !!}
        {!! $forwarded !!}
        {{--{!! @($forwarded->username ? $forwarded->username : '') !!} - {!! @($forwarded->created_at ?  \Carbon\Carbon::parse($forwarded->created_at->date)->format('d-m-Y H:i') : '' ) !!}--}}
    </div>
    <div class="time">{{ $message->created_at->diffForHumans() }}</div>
</div>