@extends('layouts.app')
@section('css')

@append

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">Conversations</div>
                    <div class="panel-body" id="users">
                        <ul class="nav nav-pills nav-stacked">
                            {{--{!! @dd($conversations); !!}--}}
                            @forelse ($conversations as $key=>$val)
                                <li><a class="active" href="#" data-uid="{{$val->withUser->id}}" id="{{$val->thread->conversation_id}}">{{$val->withUser->name}}</a></li>
                            @empty
                                <p>No users</p>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9" id="chatWrap">
                <div id="chat">
                    <div id="chat-history" class="chat-history">
                        <ul></ul>
                    </div>
                    <span id="typing"></span>
                </div>
                <form id="send-message">
                    <input type="hidden" id="destSocket" />
                    <div class="form-row">
                        <div class="form-group col-md-10">
                            <input id="message" size="80" class="form-control" autocomplete="off" placeholder="Write Message">
                        </div>
                        <div class="form-group col-md-2">
                            <input type="submit" value="Send" class="btn btn-primary btn-block" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

@append