@extends('layouts.app')
@section('content')
    <div id="chat" class="content container bootstrap snippets">
        <div class="row row-broken">
            <div class="col-sm-3 col-xs-12">
                <div class="col-inside-lg decor-default chat" tabindex="5000">
                    <h4 class="text-center">Conversations</h4>
                    <div class="chat-users" id="users">
                        <div v-on:click="getOpenConversation(conversation.id, conversation.user_id)" v-for="conversation in conversations" :key="conversation.id" :class="{ active: conversation.isActive, 'online': (online[conversation.user_id]!== undefined) }" class="user" :data-uid="conversation.user_id" :id="conversation.id">
                            <div class="avatar">
                                <img src="https://www.gravatar.com/avatar/" :alt="conversation.name">
                                <div class="status"></div>
                            </div>
                            <div class="name">@{{ conversation.name }}  <span class="badge" v-show="(conversation.count>0)">@{{ conversation.count }}</span></div>
                            <div class="mood">@{{ conversation.last_message }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-xs-12 chat"  tabindex="5001">
                <div class="decor-default">
                    <div class="chat-header col-inside-lg">
                        @{{ active_user_name }} - @{{ ((online[active_user_id]!== undefined) ? 'online' : 'offline') }} @{{ (active_user_typing ? 'is typing...' : '') }}
                    </div>
                    <div ref="messageDisplay" class="chat-body col-inside-lg" v-chat-scroll="{always: false}">
                        <div v-for="message in orderedMessages" v-observe-visibility="visibilityChanged" :id="message.id" :class="{is_seen: (message.is_seen==1), 'right sender': (message.user_id==user_id), 'left receiver': (message.user_id!=user_id), loaded: (message.previews!=''), }" class="answer">
                            <div class="message-actions btn-group">
                                <div class="btn-group inline pull-left">
                                    <a class="message-forward btn btn-secondary" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-share" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="btn-group inline pull-left dropdown">

                                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a data-type="dfm" class="message-action dropdown-item" href="#">Delete for myself</a></li>
                                        <li v-show="(message.user_id==user_id)"><a data-type="dfe" class="message-action dropdown-item" href="#">Delete for everyone</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="avatar">
                                <img src="https://www.gravatar.com/avatar/" :alt="message.id">
                            </div>
                            <div class="name">@{{message.sender.name}}</div>
                            <div class="text">
                                @{{message.message}}
                                <div class="message-attachments"></div>
                                <div class="message-previews">
                                    <a v-for="preview in message.previews" target="_blank" :href="preview.url" class="link-preview">
                                        <img :src="preview.image" :title="preview.title"/>
                                        <span class="title">@{{ preview.title }}</span>
                                        <span class="host">@{{ preview.host }}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="time">@{{ message.created_at}}</div>
                        </div>
                    </div>
                    <span id="typing"></span>
                    <div class="answer-add">
                        <form @keydown.enter.exact.prevent
                              @keyup.enter.exact="sendMessage"
                              @keydown.enter.shift.exact="newline" method="POST" action="http://localhost/jobi_market/public/messages" accept-charset="UTF-8" id="send-message" novalidate="novalidate" enctype="multipart/form-data" class="form">
                            <input v-model="active_user_id" type="hidden" id="destSocket" />
                            <input v-model="active_conversation" type="hidden" id="conversationId" />
                            <textarea v-model="message" id="message" placeholder="Write a message"></textarea>
                            <input @click.prevent="sendMessage" type="submit" class="btn btn-primary btn-sm" value="send">

                            <input @change="sendFiles" id="filesupload" ref="filesupload" type="file" name="file" multiple class="">
                        </form>
                    </div>
                </div>
                <div class="message-fwd-modal modal modal-dialog modal-dialog-centered" role="dialog" data-toggle="modal" data-target="#exampleModalCenter">
                    <header>
                        <span>Forward to:</span><a href="#" class="message-fwd-modal-close">close</a>
                    </header>

                    <div class="chat-users container-fluid" id="forwardToUsers">
                        {!! Form::open(
                                    array(
                                        'class' => 'form',
                                        'id' => 'message-forwarding',
                                        'novalidate' => 'novalidate')) !!}
                        <input type="hidden" name="message_id" id="message_id"/>
                        <div v-for="conversation in conversations" v-show="(conversation.id != active_conversation)" :key="conversation.id" :class="{ active: conversation.isActive, 'online': conversation.online }" class="user" :data-uid="conversation.user_id" :id="'forwardto-'+conversation.id" class="row forwardtouser">
                            <div class="col-xs-1">
                                <input type="checkbox" class="custom-control-input" :data-cid="conversation.id"  :data-uid="conversation.user_id" :id="'forwardCheck'+conversation.user_id">
                            </div>
                            <div class="col-xs-11">
                                <div class="user" :data-cid="conversation.id" :data-uid="conversation.user_id" :id="conversation.id">

                                    <label class="custom-control-label" :for="'forwardCheck'+conversation.user_id">
                                        <div class="avatar">
                                            <img src="https://www.gravatar.com/avatar/" :alt="conversation.name">
                                            <div class="status"></div>
                                        </div>
                                        <div class="name">@{{ conversation.name }}</div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-sm" value="send">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script>
        var timer = null;

        Vue.use(VueSocketio, 'http://'+window.location.hostname+':3000');
        var app = new Vue({
            sockets: {
                usernames: function (data) {
                    app.online = data;
                },
                updateTyping: function (data) {
                    if(this.active_user_id == data.user_id){
                        this.active_user_typing = data.typing;
                    }
                    this.conversations[data.user_id].typing = data.typing;
                },
                new_message: function (data) {
                    app.getNewMessagesCount();
                    if(data.conversationId == this.active_conversation){
                        this.$set(this.messages, data.message.id, data.message);
                        if(data.message.has_previews == 1){
                            axios.get('{{ route('messages.getpreviews') }}/'+data.message.id).then(function (response) {
                                app.messages[data.message.id].previews = response.data;
                                messageDisplay.scrollTop = messageDisplay.scrollHeight;
                            });
                        }

                        messageDisplay.scrollTop = messageDisplay.scrollHeight;
                    } else {
                        this.conversations[data.user_id].count = data.newMessagesCount;
                    }
                }

            },
            el: "#app",
            data: {
                user_id: user_id,
                conversations: [],
                messages: [],
                online: [],
                newmessages: 0,
                message: '',
                active_conversation: 0,
                active_user_name: '',
                active_user_id: 0,
                active_user_online: false,
                active_user_typing: false,
                files: []
            },
            watch: {
                message: function () {
//                    this.active_user_typing = this.message!=='';
                    app.$socket.emit('typing',
                        {
                            typing:this.message!=='',
                            from:socket.id,
                            to: this.active_user_id
                        });
                }
            },
            methods: {
                getConversations: function () {
                    return axios.get('{{ route('messages.get-conversations')}}'
                    ).then(function (response) {
                        app.conversations = response.data;
                    });
                },

                getConversationMessages: function (conv_id) {
                    return axios.get('{{ route('messages.get-conversation-messages')}}/' + conv_id
                    ).then(function (response) {
                        app.messages = response.data;
                    });
                },
                getNewMessagesCount: function () {
                    return axios.get('{{ route('messages.get-new-messages-count')}}').then(function (response) {
                        app.newmessages = response.data;
                    });
                },
                getOpenConversation: function (conv_id, user_id) {
                    this.getConversationMessages(conv_id);
                    this.active_conversation = conv_id;
                    this.active_user_id = user_id;
                    this.active_user_name = this.conversations[user_id].name;
                    this.active_user_online = ( this.online[user_id]!== undefined ? true : false );

                },
                newline: function () {
                    this.message = "${this.value}\n";
                },
                sendMessage: function () {
                    if(this.message!=''){
                    var data = {
                        message: _.escape(this.message),
                        user_id: user_id,
                        username: username,
                        conversation_id: this.active_conversation,
                        dest: this.active_user_id
                    };
                    axios.post('{{ route('messages.add') }}/'+this.active_conversation, data)
                        .then( function (response) {
                            var id = response.data.message.id;
                            app.message = '';
//                            app.messages.push(response.data.message);
                            app.$set(app.messages, id, response.data.message);
                            app.$socket.emit('send_message', {message: response.data.message, dest: app.active_user_id, conversationId: app.active_conversation, newMessagesCount: response.data.newMessagesCount});

                            axios.get('{{ route('messages.getpreviews') }}/'+id).then(function (response) {
                                app.messages[id].previews = response.data;
                                messageDisplay.scrollTop = messageDisplay.scrollHeight;
                            });
                            messageDisplay.scrollTop = messageDisplay.scrollHeight;
                        });
                    }
                },
                sendFiles: function () {
                    app.files = this.$refs.filesupload.files;
//                    console.log(app.files);
                    if(app.files.length>0){
                        var msg = {};
                        msg.files = true;
                        msg.conversation_id = app.active_conversation;
                        msg.user_id         = user_id;
                        msg.text            = '';
                        msg.destination     = app.active_user_id;

                        axios.post('{{ route('messages.add') }}/'+this.active_conversation, msg)
                            .then( function (response) {
//                                console.log(response.data.message.id);
                                var id = response.data.message.id;
                                var file_data = app.files;
                                console.log(app.files);
//                                var len = file_data.length;
                                {{--var url = baseUrl + '{{ route('messages.fileupload') }}';--}}
                                var form_data = new FormData();

                                Object.keys(msg).forEach(function (key) {
                                    form_data.append(key, msg[key]);
                                });
                                console.log(app.files.length);
                                form_data.set('id', id);
                                for (var i = app.files.length - 1; i >= 0; i--) {
                                    form_data.set('file', app.files[i]);
                                    axios.post('{{ route('messages.fileupload') }}', form_data).then(function (response) {
                                        if(i==0){
//                                            socket.emit('send message', {id: msg.id, newMessagesCount: msg.newMessagesCount, dest: msg.destination, conversationId: msg.conversation_id});
                                            app.$socket.emit('send_message', {id: id, dest: app.active_user_id, conversationId: app.active_conversation, newMessagesCount: response.data.newMessagesCount});
                                        }
                                    });
                                }

                            });
                    }
                },
                visibilityChanged: function (isVisible, entry) {
                    if(isVisible==true){
                        var m = this.messages[entry.target.id];
                        if(m.user_id !== user_id && m.is_seen == 0){
                            this.messages[entry.target.id].is_seen = 1;
                            axios.post('{{ route('messages.fetch') }}/'+m.id, {user_id: user_id}).then(function (response) {
                                app.conversations[response.data.conversationId].count = response.data.newMessagesCount;
                                app.getNewMessagesCount();
                            });
                        }
                    }
                }
            },

            created: function () {
                this.$socket.emit('new_user', {username: username, user_id: user_id, user_avatar: user_avatar, user_email: user_email, user_email_hash: user_email_hash});
                this.getConversations().then(function () {
                    var first_conversation_key  = Object.keys(app.conversations)[0];
//                    console.log(app.conversations[first_conversation_key].id);
                    app.getOpenConversation(app.conversations[first_conversation_key].id, app.conversations[first_conversation_key].user_id);
                    app.getNewMessagesCount();
                });
            },
            computed: {
                orderedMessages: function () {
                    return _.orderBy(this.messages, 'created_at')
                }
            }

        });
        var messageDisplay = app.$refs.messageDisplay;
    </script>
@endsection