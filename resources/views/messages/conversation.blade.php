{{--{{@dd($conversation->messages)}}--}}
@forelse ($conversation->messages as $message)
    {{--@if($message->sender->id!=Auth::user()->id && $message->is_seen == 0) {{\App\Models\Message::makeSeen($message->id)}} @endif;--}}
    {{--<li><b>{{$message->sender->name}}:</b> {{$message->message}}</li>--}}
    <div class="answer @if (Auth::user()->id == $message->sender->id) right @else left @endif" id="{{$message->id}}">
        <div class="avatar">
            <img src="https://www.gravatar.com/avatar/{{@md5($message->sender->email)}}" alt="{{$message->sender->name}}">
            {{--<div class="status offline"></div>--}}
        </div>
        <div class="name">{{$message->sender->name}}</div>
        <div class="text">
            {{$message->message}}
        </div>
        <div class="time">{{ $message->created_at->diffForHumans() }}</div>
    </div>
@empty

@endforelse