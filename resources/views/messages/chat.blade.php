@extends('layouts.layout')
@section('content')
    <!-- Chat app -->
    <div class="block_whole content_msg" id="chat">
        <div class="row flex">
            <div class="col s12 l3 xl2 right_whole">

                <div class="top_right_whole">
                    <div class="btn_message_setting">
                        <figure>
                            <img src="images/20.jpg" alt="">
                            <span class="tip_sesion user_online"></span>
                        </figure>
                        <button type="button" class="new_msg"><span class="new_notification"></span><i
                                    class="material-icons">local_post_office</i>
                        </button>
                        <button type="button" class="show_selected_msg"><i class="material-icons">star</i>
                        </button>
                    </div>
                    <div class="search_events">
                        <input type="search" placeholder="Поиск">
                        <button type="submit"><i class="material-icons">search</i></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="list_users_msg style-scroll">
                    <div class="content_user_msg">
                        <ul>
                            <li @click.prevent="getOpenConversation(conversation.id, conversation.user_id)" v-for="conversation in orderedConversations" :key="conversation.id" :class="{ active: (conversation.id==active_conversation) }" class="user" :data-uid="conversation.user_id" :id="conversation.id">
                                <a href="#"></a>
                                <button class="slect_star" type="button"><i
                                            class="material-icons">star</i></button>
                                <figure class="avatar_user_msg"><img :src="conversation.user_avatar" :alt="conversation.name">
                                    <span :class="[(online[conversation.user_id]!== undefined) ? 'user_online' : 'user_offline' ]"  class="tip_sesion"></span>
                                </figure>
                                <p>
                                    <span class="data_msg">@{{ (conversation.last_message_date.date!='' ? conversation.last_message_date.date : conversation.last_message_date.time) }}</span>
                                    <span class="nr_count_left" v-if="(conversation.count>0)">@{{ conversation.count }}</span>
                                    <strong class="name_user_msg">@{{ conversation.name }}</strong>
                                    <span class="last_msg">@{{ conversation.last_message | truncate(20) }}</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 l9 xl10 left_whole">

                <div class="top_left_whole">
                    <h3 :class="[(online[active_user_id]!== undefined) ? 'user_is_online' : '']" class="name_user_typing">@{{ active_user_name }}</h3>
                    <div class="event_now_typing" v-show="active_user_typing">Пишет...</div>
                    <div class="right option_user_typing">
                        <!-- Dropdown Trigger -->
                        <a class='dropdown-button btn waves-effect waves-blue drop_menu_js' href='#'
                           data-activates='dropdown1'><i
                                    class="material-icons">menu</i></a>
                        <!-- Dropdown Structure -->
                        <ul id='dropdown1' class='dropdown-content dropdown_chat_setting'>
                            <li><a href="#!">Перейти к профилю</a></li>
                            <li><a href="#!">Заблокировать</a></li>
                            <li><a href="#!">Скрыть чат</a></li>
                            <li><a href="#!">Очистить чат</a></li>
                            <li><a href="#!">Не беспокойте меня</a></li>
                        </ul>
                    </div>
                </div>

                <div class="all_chat">
                    <div class="forward">
                        <div class="top_forwar">
                            <h3>Выбираете кому переслать сообщение</h3>
                            <button class="close_forward disable_forward" type="button"><i class="material-icons">close</i></button>
                        </div>
                        <div class="list_users_msg style-scroll">
                            <div class="content_user_msg">
                                <ul>
                                    <li v-show="(conversation.user_id !== active_user_id)" v-for="conversation in orderedConversations" :key="'forwardto_'+conversation.id" class="user" :id="'forwardto_'+conversation.id">
                                        <input type="checkbox" class="filled-in" :id="'filled-'+conversation.id"/>
                                        <label :for="'filled-'+conversation.id"></label>
                                        <figure class="avatar_user_msg"><img :src="conversation.user_avatar" :alt="conversation.name">
                                            <span :class="[(online[conversation.user_id]!== undefined) ? 'user_online' : 'user_offline' ]"  class="tip_sesion"></span>
                                        </figure>
                                        <p><strong class="name_user_msg">@{{ conversation.name }}</strong></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer_forward">
                            <button class="disable_forward btn waves-effect waves-blue red" type="button">Отмена</button>
                            <button class="btn waves-effect waves-blue blue" type="button">Переслать</button>
                        </div>
                    </div>
                    <div class="conversation_content style-scroll">
                        <!-- chat body -->
                        <div class="chat-body col-inside-lg">
                            <!-- Messages loop -->
                            <div v-for="message in orderedMessages" v-observe-visibility="visibilityChanged" :id="message.id" :class="{is_seen: (message.is_seen==1), 'right sender': (message.user_id==user_id), 'left receiver': (message.user_id!=user_id), loaded: (message.previews!=''), }" class="answer">
                                <div v-if="(message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0)" class="message-actions">
                                    <div class="msg_option">
                                        <!-- Dropdown Trigger -->
                                        <a :class="{'actions_left': (message.user_id==user_id), 'actions_right': (message.user_id!=user_id)}" class='dropdown-button btn waves-effect waves-blue drop_menu_chat drop_msg_js ' href='#' :data-activates="'dropdown'+message.id">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <!-- Dropdown Structure -->
                                        <ul :id="'dropdown'+message.id" class='dropdown-content dropdown_chat_setting'>
                                            <li><a href="#!">Копировать сообщения</a></li>
                                            <li><a class="add_forwar" href="#!">Переслать</a></li>
                                            <li><a @click="deleteMessage(message.id,'dfm')" href="#!">Удалить для меня</a></li>
                                            <li v-show="(message.user_id==user_id)"><a @click="deleteMessage(message.id,'dfe')" href="#!">Удалить для всех</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div v-if="(message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0)" class="avatar"><img :src="'{{asset('storage')}}/'+message.sender.avatar" :alt="message.sender.name">
                                </div>
                                <div class="name">@{{ ((message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0)) ? message.sender.name : 'Message was deleted'}}</div>

                                <div class="text">
                                    <p>@{{ ((message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0) ? message.message : 'Message deleted')  }}</p>
                                    <div v-if="(message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0)" v-for="preview in message.previews" class="link_chat">
                                        <span class="link_long">https://www.youtube.com/watch?v=oR-UNs_IugU</span>
                                        <a target="_blank" :href="preview.url" class="link-preview">
                                            <img :src="preview.image" :alt="preview.title">
                                            <span class="title">@{{ preview.title }}</span>
                                            <span class="host">@{{ preview.host }}</span>
                                        </a>
                                    </div>
                                    <div v-if="(message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0)" class="message-attachments" v-show="(message.messages_attachments && message.messages_attachments.length!=0)">

                                        <div v-for="attachment in message.messages_attachments" class="message-attachment">
                                            <a v-if="isVideo(attachment.mime)" class="attachment-item mp4 video-btn modal-trigger pulse" href="#message-video" @click="message_video = '{{ asset('storage') }}/'+attachment.filename">
                                                <i class="fa fa-play"></i>
                                            </a>
                                            <video v-if="isVideo(attachment.mime)" :src="'{{ asset('storage') }}/'+attachment.filename"></video>

                                            <a v-if="isImage(attachment.mime)" :data-fancybox="'gallery'+message.id" :href="'{{asset('storage')}}/'+attachment.filename" class="attachment-item jpg">
                                                <img :src="'{{asset('storage')}}/'+attachment.filename">
                                            </a>
                                            <div v-if="(isImage(attachment.mime)===false) && isVideo(attachment.mime)===false" class="file_format"><a target="_blank" :href="'{{asset('storage')}}/'+attachment.filename"></a>
                                                <div class="name_file">@{{ attachment.mime }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div v-if="(message.user_id!==user_id && message.deleted_from_receiver===0) || (message.user_id===user_id && message.deleted_from_sender===0)" class="time">22 hours ago</div>
                            </div>
                            <!-- end of Messages loop -->

                            <div v-show="active_user_typing" class="answer left">
                                <div class="avatar"><img :src="active_user_avatar" :alt="active_user_name"></div>
                                <div class="name">@{{active_user_name}}</div>
                                <div class="text">
                                    <div class="bubblingG">
                                        <span id="bubblingG_1"></span>
                                        <span id="bubblingG_2"></span>
                                        <span id="bubblingG_3"></span>
                                    </div>
                                </div>
                                <div class="time"><div class="hour_chat"><i class="material-icons">alarm</i>Пишет сейчас...</div></div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <!-- .chat body -->
                    </div>
                </div>

                <div class="add_msg_editor">
                    <form @keydown.enter.exact.prevent
                          @keyup.enter.exact="sendMessage"
                          @keydown.enter.shift.exact="newline"
                          accept-charset="UTF-8" id="send-message" novalidate="novalidate" enctype="multipart/form-data" class="form" action="">
                        <input v-model="active_user_id" type="hidden" id="destSocket" />
                        <input v-model="active_conversation" type="hidden" id="conversationId" />
                        <textarea v-model="message" id="message" placeholder="Введите текст, чтобы отправить..."></textarea>
                        <label class="btn_add_file waves-effect waves-blue" for="add_img_chat">
                            <span><i class="material-icons">attach_file</i></span>
                            <input @change="sendFiles" id="filesupload" ref="filesupload" type="file" name="file" multiple type="file" >
                        </label>
                        <button @click.prevent="sendMessage" class="send_msg waves-effect waves-blue" type="submit"><i
                                    class="material-icons">send</i></button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal Structure -->
        <div id="message-video" class="modal video_modal">
            <button type="button" class="modal-action modal-close x_close"><i class="material-icons">close</i></button>
            <video id="video_resume" :src="message_video" controls></video>

        </div>
    </div>
    <!-- end of Chat app -->
@endsection

@section('js')

    <script>
        var timer = null;

        Vue.use(VueSocketio, 'http://'+window.location.hostname+':3000');
        var app = new Vue({
            sockets: {
                usernames: function (data) {
                    app.online = data;
                },
                updateTyping: function (data) {
                    if(this.active_user_id == data.user_id){
                        this.active_user_typing = data.typing;
                    }
                    this.conversations[data.user_id].typing = data.typing;
                },
                new_message: function (data) {
                    // console.log(data);
                    app.getNewMessagesCount();
                    if(data.conversationId == this.active_conversation){
                        this.$set(this.messages, data.message.id, data.message);
                        if(data.message.has_previews == 1){
                            axios.get('{{ route('messages.getpreviews') }}/'+data.message.id).then(function (response) {
                                app.messages[data.message.id].previews = response.data;
                            });
                        }

                    } else {
                        this.conversations[data.user_id].count = data.newMessagesCount;
                    }
                },
                message_dfe: function (data){
                    app.last_method = 'deleteMessage';
                    if(app.messages[data.id]){
                        app.messages[data.id].deleted_from_receiver = 1;
                        app.messages[data.id].deleted_from_sender = 1;
                    }
                }

            },
            el: "#chat",
            data: {
                img_mimes: ['jpeg','png','jpg','gif','svg'],
                video_mimes: ['mp4','ogx','oga','ogv','ogg','webm'],
                message_video: '{{asset('video/video_cv.mp4')}}',
                user_id: user_id,
                conversations: [],
                messages: [],
                online: [],
                newmessages: 0,
                message: '',
                active_conversation: 0,
                active_user_name: '',
                active_user_avatar: '',
                active_user_id: 0,
                active_user_online: false,
                active_user_typing: false,
                files: [],
                last_method: ''
            },
            watch: {
                message: function () {


//                    this.active_user_typing = this.message!=='';
//                     console.log('typing');
                    app.$socket.emit('typing',
                        {
                            typing:true,
                            // from:socket.id,
                            to: this.active_user_id
                        });
                    this.expensiveOperation();
                },
                messages: function () {
                    app.rebindMessageDropdown();
                }
            },
            methods: {
                // This is where the debounce actually belongs.
                expensiveOperation: _.debounce(function () {
                    // this.isCalculating = true
                    setTimeout(function () {
                        // this.isCalculating = false
                        // this.searchQueryIsDirty = false
                        app.$socket.emit('typing',
                            {
                                typing:false,
                                // from:socket.id,
                                to: this.active_user_id
                            });
                    }.bind(this), 1000)
                }, 500),
                getConversations: function () {
                    return axios.get('{{ route('messages.get-conversations')}}'
                    ).then(function (response) {
                        app.conversations = response.data;
                    });
                },

                getConversationMessages: function (conv_id) {
                    return axios.get('{{ route('messages.get-conversation-messages')}}/' + conv_id
                    ).then(function (response) {
                        app.messages = response.data;
                    });
                },
                getNewMessagesCount: function () {
                    return axios.get('{{ route('messages.get-new-messages-count')}}').then(function (response) {
                        app.newmessages = response.data;
                    });
                },
                getOpenConversation: function (conv_id, user_id) {
                    if(conv_id != this.active_conversation){
                        this.getConversationMessages(conv_id);
                        this.active_conversation = conv_id;
                        this.active_user_id = user_id;
                        this.active_user_name = this.conversations[user_id].name;
                        this.active_user_avatar = this.conversations[user_id].user_avatar;
                        this.active_user_online = ( this.online[user_id]!== undefined ? true : false );

                        app.last_method = 'getOpenConversation';
                    }
                },
                newline: function () {
                    this.message = "${this.value}\n";
                },
                sendMessage: function () {
                    if(this.message.length!=0){
                        var data = {
                            message: _.escape(this.message),
                            user_id: user_id,
                            username: username,
                            conversation_id: this.active_conversation,
                            dest: this.active_user_id
                        };
                        axios.post('{{ route('messages.add') }}/'+this.active_conversation, data)
                            .then( function (response) {
                                var id = response.data.message.id;
                                app.message = '';
                                console.log(response.data.message);
                           // app.messages.push(response.data.message);
                                app.$set(app.messages, id, response.data.message);
//                                 app.messages[id] = response.data.message;
                                app.$socket.emit('send_message', {message: response.data.message, dest: app.active_user_id, conversationId: app.active_conversation, newMessagesCount: response.data.newMessagesCount});

                                axios.get('{{ route('messages.getpreviews') }}/'+id).then(function (response) {
                                    app.messages[id].previews = response.data;
                                });
                            });
                    }
                    app.last_method = 'sendMessage';
                },
                deleteMessage: function (id, type) {
                    var data = {};
                    data.user_id        = user_id;
                    data.message_id     = id;
                    data.type           = type;
                    axios.post('{{ route('messages.delete') }}', data)
                        .then(function (result) {
                            var id      = result.data.message_id,
                                user_id = result.data.user_id,
                                type    = result.data.type;
                            if(type==='dfe'){
                                app.messages[id].deleted_from_receiver = 1;
                                app.messages[id].deleted_from_sender = 1;
                                app.$socket.emit('message_dfe', {to: app.active_user_id, id: id, type: type, cid: app.active_conversation});
                            } else {
                                app.messages[id].deleted_from_receiver = 1;
                                app.messages[id].deleted_from_sender = 1;
                            }
                        });
                    app.last_method = 'deleteMessage';
                },
                sendFiles: function () {
                    app.files = this.$refs.filesupload.files;
                    if(app.files.length>0){
                        var msg = {};
                        msg.files = true;
                        msg.conversation_id = app.active_conversation;
                        msg.user_id         = user_id;
                        msg.text            = '';
                        msg.destination     = app.active_user_id;

                        axios.post('{{ route('messages.add') }}/'+this.active_conversation, msg)
                            .then( function (response) {
                                var id = response.data.message.id;
                                var file_data = app.files;
                                var form_data = new FormData();
                                form_data.set('id', id);
                                form_data.set('conversation_id', app.active_conversation);

                                for( var i = 0; i < app.files.length; i++ ){
                                    var file = app.files[i];
                                    form_data.append('files[' + i + ']', file);
                                }
                                axios.post('{{ route('messages.fileupload') }}', form_data).then(function (results) {
                                    app.$set(app.messages, id, results.data.message);

                                    console.log(results.data);
                                    app.$socket.emit('send_message', {message:  results.data.message, dest: app.active_user_id, conversationId: app.active_conversation, newMessagesCount: results.data.newMessagesCount});
                                }).catch(function (error) {
                                    console.log(error);
                                });

                            });
                    }
                    app.last_method = 'sendFiles';
                },
                visibilityChanged: function (isVisible, entry) {
                    if(isVisible==true){
                        console.log(entry);
                    var m = this.messages[entry.target.id];
                        if(m.user_id !== user_id && m.is_seen == 0){
                            axios.post('{{ route('messages.fetch') }}/'+m.id, {user_id: user_id}).then(function (response) {
                                // console.log(app.messages[entry.target.id].message);
                                app.messages[entry.target.id].is_seen = 1;
                                app.conversations[response.data.user_id].count = response.data.newMessagesCount;
                                // console.log(app.conversations[response.data.conversationId].count);
                                // console.log(app.conversations[response.data.conversationId].count);
                                app.getNewMessagesCount();
                            });
                        }
                    }
                },
                isImage: function (mime) {
                    // console.log(mime+'-'+(app.img_mimes.contains(mime))+'- image');
                    return (app.img_mimes.indexOf(mime) > -1);
                },
                isVideo: function (mime) {
                    // console.log((mime in app.img_mimes)+'- video');
                    return (app.video_mimes.indexOf(mime) > -1);
                },
                rebindMessageDropdown: function() {
                    $('.actions_right').dropdown({
                            inDuration: 300,
                            outDuration: 225,
                            constrainWidth: false, // Does not change width of dropdown to that of the activator
                            hover: true, // Activate on hover
                            gutter: 0, // Spacing from edge
                            belowOrigin: false, // Displays dropdown below the button
                            alignment: 'left', // Displays dropdown with edge aligned to the left of button
                            stopPropagation: false // Stops event propagation
                        }
                    );

                    $('.actions_left').dropdown({
                            inDuration: 300,
                            outDuration: 225,
                            constrainWidth: false, // Does not change width of dropdown to that of the activator
                            hover: true, // Activate on hover
                            gutter: 0, // Spacing from edge
                            belowOrigin: false, // Displays dropdown below the button
                            alignment: 'right', // Displays dropdown with edge aligned to the left of button
                            stopPropagation: false // Stops event propagation
                        }
                    );
                },
                scrollConversation: function(){

                    if(app.last_method !== 'deleteMessage'){
                        $(".conversation_content").scrollTop($(".conversation_content")[0].scrollHeight);
                    }
                }
            },

            created: function () {
                // var messageDisplay = app.$refs.messageDisplay;

                this.$socket.emit('new_user', {username: username, user_id: user_id, user_avatar: user_avatar, user_email: user_email, user_email_hash: user_email_hash});
                this.getConversations().then(function () {
                    var first_conversation_key  = Object.keys(app.orderedConversations)[0];
//                    console.log(app.conversations[first_conversation_key].id);
                    app.getOpenConversation(app.orderedConversations[first_conversation_key].id, app.orderedConversations[first_conversation_key].user_id);
                    app.getNewMessagesCount();
                });
            },
            updated: function (){
                app.scrollConversation();
                app.rebindMessageDropdown();
            },
            ready: function() {

            },
            computed: {
                orderedMessages: function () {
                    return _.orderBy(this.messages, 'id')
                },
                orderedConversations: function () {
                    return _.orderBy(this.conversations, 'updated_at', 'desc')
                }
            },
            filters: {
                truncate: function(string, value) {
                    if(string && string.length>value){
                        return string.substring(0, value) + '...';
                    }
                    return string;
                }
            }

        });
    </script>
@endsection