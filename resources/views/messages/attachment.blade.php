<div class="message-attachment">
    <a target="_blank" href="{{ asset("storage")}}/{{$file->filename}}" class="attachment-item {{$file->mime}}">
        @if(in_array($file->mime, $file->img_mimes))
            <img src="{{ asset("storage")}}/{{$file->filename}}"/>
        @elseif((in_array($file->mime, $file->video_mimes)))
            <video src="{{ asset("storage") }}/{{$file->filename}}"></video>
        @else
            <div class="message-attachment-filebox"><span>{{$file->mime}}</span></div>
        @endif
    </a>
</div>