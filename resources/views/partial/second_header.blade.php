<!-- Start Header -->
<header>
    <div class="row content_header">
        <div class="col l6">
            <button class="menu_do left waves-effect waves-blue" id="do_mini_aside" type="button"><span
                        class="icon-menu"></span>
            </button>
            <div class="breadscrumb left">
                <ul>
                    <li><a href="#"><i class="material-icons">dashboard</i> Dashboard</a></li>
                    <li>Создать резюме</li>
                </ul>
            </div>
        </div>
        <div class="col l6">
            <div class="right-align">
                <div class="nav_top">
                    <ul>
                        <li><span class="nr_count">5</span>
                            <a id="show_events" class="waves-effect waves-blue simple_events"
                               href="javascript:;"><i
                                        class="material-icons">notifications</i></a>



                        </li>
                        <li><span class="nr_count">1</span><a id="show_guest" class="waves-effect waves-blue simple_events" href="javascript:;"><i class="material-icons">group</i></a>
                        </li>
                        <li><span class="nr_count">+9</span><a class="waves-effect waves-blue simple_events"
                                                               href="#"><i
                                        class="material-icons">local_post_office</i></a></li>

                    </ul>
                </div>
                <div class="btn_setting">
                    <button id="right_setting_add" type="button" class="waves-effect waves-blue"><i
                                class="material-icons">settings</i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header -->