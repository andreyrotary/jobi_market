<div class="chat-body col-inside-lg">
    <div class="answer loaded  right sender  is_seen">
        <div class="message-actions">
            <div class="msg_option">
                <!-- Dropdown Trigger -->
                <a class='dropdown-button btn waves-effect waves-blue drop_menu_chat drop_msg_js' href='#'
                   data-activates='dropdown2'>
                    <i class="material-icons">more_vert</i>
                </a>
                <!-- Dropdown Structure -->
                <ul id='dropdown2' class='dropdown-content dropdown_chat_setting'>
                    <li><a href="#!">Копировать сообщения</a></li>
                    <li><a class="add_forwar" href="#!">Переслать</a></li>
                    <li><a href="#!">Удалить от меня</a></li>
                    <li><a href="#!">Удалить от всех</a></li>
                </ul>
            </div>
        </div>
        <div class="avatar"><img src="images/20.jpg" alt="Orange">
        </div>
        <div class="name">Orange</div>


        <div class="text">

            <div class="link_chat">
                <span class="link_long">https://www.youtube.com/watch?v=oR-UNs_IugU</span>
                <a target="_blank" href="https://www.youtube.com/watch?v=oR-UNs_IugU" class="link-preview">
                    <img src="https://i.ytimg.com/vi/oR-UNs_IugU/hqdefault.jpg">
                    <span class="title">Coronita (Best Of 2013) House-Tech house { Dj Meon }</span>
                    <span class="host">https://www.youtube.com</span>
                </a>
            </div>
            <span class="from_how">"<strong>Отправлено:</strong> Samsung - 23-01-2018 11:25"</span>

        </div>
        <div class="time">22 hours ago</div>
    </div>


    <div class="answer   left receiver  is_seen">
        <div class="message-actions">
            <div class="msg_option">
                <!-- Dropdown Trigger -->
                <a class='dropdown-button btn waves-effect waves-blue drop_menu_chat drop_msg_js' href='#'
                   data-activates='dropdown3'>
                    <i class="material-icons">more_vert</i>
                </a>
                <!-- Dropdown Structure -->
                <ul id='dropdown3' class='dropdown-content dropdown_chat_setting'>
                    <li><a href="#!">Копировать сообщения</a></li>
                    <li><a href="#!">Переслать</a></li>
                    <li><a href="#!">Удалить от меня</a></li>
                    <li><a href="#!">Удалить от всех</a></li>
                </ul>
            </div>
        </div>
        <div class="avatar"><img src="images/10.jpg" alt="Goncear Elena">
        </div>
        <div class="name">Goncear Elena</div>
        <div class="text">
            <div class="message-attachments">
                <div class="message-attachment">

                    <a class="attachment-item mp4 video-btn modal-trigger pulse" href="#video">
                        <i class="fa fa-play"></i>

                    </a>
                    <video src="video/video_cv.mp4"></video>
                </div>
                <div class="message-attachment">
                    <a data-fancybox="gallery1" href="images/37.jpg" class="attachment-item jpg">
                        <img src="images/37.jpg">
                    </a>
                </div>
                <div class="message-attachment">
                    <a data-fancybox="gallery1" href="images/38.jpg" class="attachment-item jpg">
                        <img src="images/38.jpg">
                    </a>
                </div>
            </div>
        </div>
        <div class="time">21 hours ago</div>
    </div>


    <div class="answer loaded  right sender  is_seen">
        <div class="message-actions">
            <div class="msg_option">
                <!-- Dropdown Trigger -->
                <a class='dropdown-button btn waves-effect waves-blue drop_menu_chat drop_msg_js' href='#'
                   data-activates='dropdown4'>
                    <i class="material-icons">more_vert</i>
                </a>
                <!-- Dropdown Structure -->
                <ul id='dropdown4' class='dropdown-content dropdown_chat_setting'>
                    <li><a href="#!">Копировать сообщения</a></li>
                    <li><a href="#!">Переслать</a></li>
                    <li><a href="#!">Удалить от меня</a></li>
                    <li><a href="#!">Удалить от всех</a></li>
                </ul>
            </div>
        </div>
        <div class="avatar"><img src="images/20.jpg" alt="Orange">
        </div>
        <div class="name">Orange</div>


        <div class="text">

            <div class="link_chat">
                <span class="link_long">https://www.facebook.com/triumfunitried/</span>
                <a target="_blank" href="https://www.facebook.com/triumfunitried/" class="link-preview">
                    <img
                            src="https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-1/p200x200/17457306_193125134511926_7916232335749572354_n.jpg?oh=e0d5c0661c8f40f5db8362bba8f0437c&amp;oe=5ADAFC61">
                    <span class="title">Triumf-Unitried SRL</span>
                    <span class="host">https://www.facebook.com</span>
                </a>
            </div>
            <span class="from_how">"<strong>Отправлено:</strong> Samsung - 22-01-2018 19:03"</span>

        </div>
        <div class="time">21 hours ago</div>
    </div>


    <div class="answer   left receiver  is_seen">
        <div class="message-actions">
            <div class="msg_option">
                <!-- Dropdown Trigger -->
                <a class='dropdown-button btn waves-effect waves-blue drop_menu_chat drop_msg_js' href='#'
                   data-activates='dropdown3'>
                    <i class="material-icons">more_vert</i>
                </a>
                <!-- Dropdown Structure -->
                <ul id='dropdown3' class='dropdown-content dropdown_chat_setting'>
                    <li><a href="#!">Копировать сообщения</a></li>
                    <li><a href="#!">Переслать</a></li>
                    <li><a href="#!">Удалить от меня</a></li>
                    <li><a href="#!">Удалить от всех</a></li>
                </ul>
            </div>
        </div>
        <div class="avatar"><img src="images/10.jpg" alt="Goncear Elena">
        </div>
        <div class="name">Goncear Elena</div>
        <div class="text">
            <p>Размышляя, что написать обо мне в резюме, помните, эта графа должна включать те данные, которые не вошли
                в другие разделы.</p>
            <p>Конечно, в первую очередь работодатель будет смотреть на умения и стаж, но готовность к поездкам или
                владение иностранным языком может дать вам несколько дополнительных очков.</p>
        </div>
        <div class="time">21 hours ago</div>
    </div>


    <div class="answer loaded  right sender  is_seen">
        <div class="message-actions">
            <div class="msg_option">
                <!-- Dropdown Trigger -->
                <a class='dropdown-button btn waves-effect waves-blue drop_menu_chat drop_msg_js' href='#'
                   data-activates='dropdown4'>
                    <i class="material-icons">more_vert</i>
                </a>
                <!-- Dropdown Structure -->
                <ul id='dropdown4' class='dropdown-content dropdown_chat_setting'>
                    <li><a href="#!">Копировать сообщения</a></li>
                    <li><a href="#!">Переслать</a></li>
                    <li><a href="#!">Удалить от меня</a></li>
                    <li><a href="#!">Удалить от всех</a></li>
                </ul>
            </div>
        </div>
        <div class="avatar"><img src="images/20.jpg" alt="Orange">
        </div>
        <div class="name">Orange</div>


        <div class="text">
            <p>Размышляя, что написать обо мне в резюме, помните, эта графа должна включать те данные, которые не вошли
                в другие разделы.</p>
            <p>Конечно, в первую очередь работодатель будет смотреть на умения и стаж, но готовность к поездкам или
                владение иностранным языком может дать вам несколько дополнительных очков.</p>
        </div>
        <div class="time">21 hours ago</div>
    </div>

    <div class="clearfix"></div>
</div>